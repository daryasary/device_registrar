from django.conf.urls import url, include
from rest_framework.documentation import include_docs_urls

urlpatterns = [
    url(r'^devices/', include('devices.api.urls')),
    url(r'^services/', include('services.api.urls')),

    url(r'^reports/', include('reports.urls')),
    url(r'^mongo/', include('mongopanel.urls')),

    url(r'^tracker/', include('tracker.api.urls')),

    url(r'^docs/', include_docs_urls(title='Registrar')),
]
