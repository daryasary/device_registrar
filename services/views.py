from datetime import datetime, timedelta

from django.utils import timezone

from rest_framework import mixins
from rest_framework.decorators import detail_route
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from reports.reports_patterns_generator import *
from reports.utils.format import unify_carriers
from reports.utils.query import mongo_query
from reports.utils.reports_data_converter import data_converter
from services.api.serializers import ServiceListSerializer
from services.models import Service

from django.utils.translation import ugettext_lazy as _


class ServicesViewSet(
    mixins.ListModelMixin, mixins.RetrieveModelMixin,
    mixins.CreateModelMixin, GenericViewSet
):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = ServiceListSerializer

    def get_queryset(self):
        return Service.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    @detail_route(method=["post"])
    def get_token(self, *args, **kwargs):
        instance = self.get_object()
        return Response({"token": instance.get_jwt_token()})


class ReportBaseInitial(object):

    @staticmethod
    def filter_data(request):
        return request.data.get('filter_values', None)

    def get_date_filter(self, request):
        request_data = self.filter_data(request)
        if request_data is None:
            return {}
        from_date = request_data.get('start_date', None)
        to_date = request_data.get('end_date', None)

        filter_data = dict()
        if from_date:
            from_date = datetime.strptime(from_date, '%Y-%m-%d')
        else:
            day_diff = dict(hour=0, minute=0, second=0, microsecond=0)
            from_date = (timezone.now() - timedelta(days=30)).replace(
                **day_diff)

        filter_data['date'] = dict()
        filter_data['date']['$gte'] = from_date
        if to_date:
            to_date = datetime.strptime(to_date, '%Y-%m-%d')
            if 'date' not in filter_data.keys():
                filter_data['date'] = dict()
            filter_data['date']['$lt'] = to_date
        return filter_data

    def detect_service(self, request):
        request_data = self.filter_data(request)
        if request_data is not None:
            return request_data.get('Service id', 0)
        return 0

    def generate_filter_data(self, request):
        filter_data = self.get_date_filter(request)
        filter_data.update(
            dict(service=self.detect_service(request), user_id=request.user.id)
        )
        values = dict(data=1, _id=0)
        return [filter_data, values]

    @staticmethod
    def create_services_filter(request):
        services = Service.objects.filter(user=request.user)
        services_filters = [
            {
                'text': srv['package_name'],
                'value': srv['id']
            } for srv in services.values('id', 'package_name')
            ]
        return services_filters

    def generate_report_base(self, request):
        report = generate_report_pattern(
            'Device Registrar Daily Report',
            'device_registrar_daily_report'
        )
        report['filters'].append(filter_pattern(
            'date', 'date_georgia', ['start_date', 'end_date'])
        )
        report['filters'].append(filter_pattern(
            _('Service id'), 'dropdown', self.create_services_filter(request))
        )
        return report


class ReportModuleHandler(object):
    @staticmethod
    def add_daily_devices_stacked_chart(report, data):
        devices_numbers_daily_stacked_chart = chart_pattern(
            'Daily Devices number for Services',
            'chart-linear', 'Dates', 'Devices Number'
        )
        devices_numbers_daily_stacked_chart['graph_data']['data'] = data
        report['data'].append(devices_numbers_daily_stacked_chart)

    @staticmethod
    def add_sdk_versions(report, data):
        sdk_versions_bar_chart = chart_pattern(
            'SDK versions chart', 'chart-basic-bar', 'SDK Versions', 'Y label'
        )
        sdk_versions_bar_chart['graph_data']['data'] = data.get('sdk_versions', {})
        report['data'].append(sdk_versions_bar_chart)

    @staticmethod
    def add_app_versions(report, data):
        app_versions_bar_chart = chart_pattern(
            'APP versions chart', 'chart-basic-bar', 'App Versions', 'Y label'
        )
        app_versions_bar_chart['graph_data']['data'] = data.get('app_versions', {})
        report['data'].append(app_versions_bar_chart)

    @staticmethod
    def add_client_os(report, data):
        client_os_bar_chart = chart_pattern(
            'Client os chart', 'chart-basic-bar', 'OS Name', 'Count'
        )
        client_os_bar_chart['graph_data']['data'] = data.get('os_stats', {})
        report['data'].append(client_os_bar_chart)

    @staticmethod
    def add_client_model(report, data):
        client_models_bar_chart = chart_pattern(
            'Client models chart', 'chart-basic-bar', 'Device Model', 'Count'
        )
        client_models_bar_chart['graph_data']['data'] = data.get('models_stats',{})
        report['data'].append(client_models_bar_chart)

    @staticmethod
    def add_display_size(report, data):
        display_sizes_bar_chart = chart_pattern(
            'Client display size chart', 'chart-basic-bar', 'Display sizes',
        )
        display_sizes_bar_chart['graph_data']['data'] = data.get('display_stats', {})
        report['data'].append(display_sizes_bar_chart)

    @staticmethod
    def add_carriers(report, data):
        carriers_bar_chart = chart_pattern(
            'Carriers chart', 'chart-basic-bar', 'Display sizes', 'Count'
        )
        carriers = unify_carriers(data.get('carriers_stats', {}))
        carriers_bar_chart['graph_data']['data'] = carriers
        report['data'].append(carriers_bar_chart)

    @staticmethod
    def add_languages(report, data):
        languages_bar_chart = pie_pattern('Languages pie', 'pie', 'lang-pie')
        languages_bar_chart['graph_data']['data'] = data.get('lang_stats', {})
        report['data'].append(languages_bar_chart)


class ReportBaseAPI(ReportBaseInitial, ReportModuleHandler):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def daily_reports(self, request, report=None):
        if report is None:
            report = self.generate_report_base(request)
        filter_data = self.generate_filter_data(request)
        daily_statics = mongo_query(
            'registrar_daily_stats',
            filter_data
        )
        self.add_daily_devices_stacked_chart(report, daily_statics)
        return report

    def aggregate_reports(self, request, report=None):
        if report is None:
            report = self.generate_report_base(request)
        filter_data = dict(
            service=self.detect_service(request), user_id=request.user.id
        )
        sort_key = [('date', -1)]
        aggregation_statics = mongo_query(
            'registrar_aggregate_stats', filter_data, sort=sort_key, limit=1
        )
        if len(aggregation_statics):
            aggregation_statics = aggregation_statics[0]

        self.add_sdk_versions(report, aggregation_statics)
        self.add_app_versions(report, aggregation_statics)
        self.add_client_os(report, aggregation_statics)
        self.add_client_model(report, aggregation_statics)
        self.add_display_size(report, aggregation_statics)
        self.add_carriers(report, aggregation_statics)
        self.add_languages(report, aggregation_statics)
        return report


class ReportsRequestHandler(APIView):
    allowed_methods = ['get', 'post']

    def dispatch(self, request, *args, **kwargs):
        """
        `.dispatch()` is pretty much the same as Django's regular dispatch,
        but with extra hooks for startup, finalize, and exception handling.
        """
        self.args = args
        self.kwargs = kwargs
        request = self.initialize_request(request, *args, **kwargs)
        self.request = request
        self.headers = self.default_response_headers  # deprecate?

        try:
            self.initial(request, *args, **kwargs)

            # Get the appropriate handler method
            if request.method.lower() in self.allowed_methods:
                handler = self.get_handler(request, *args, **kwargs)
            else:
                handler = self.http_method_not_allowed

            response = handler(request, *args, **kwargs)

        except Exception as exc:
            response = self.handle_exception(exc)

        self.response = self.finalize_response(request, response, *args, **kwargs)
        return self.response


class DailyReport(ReportBaseAPI, ReportsRequestHandler):
    def get_handler(self, request, *args, **kwargs):
        return self.get_daily_report

    def get_daily_report(self, request, *args, **kwargs):
        return Response(
            data_converter(self.daily_reports(request, *args, **kwargs))
        )


class AggregateReport(ReportBaseAPI, ReportsRequestHandler):
    def get_handler(self, request, *args, **kwargs):
        return self.get_aggregate_reports

    def get_aggregate_reports(self, request, *args, **kwargs):
        return Response(
            data_converter(self.daily_reports(request, *args, **kwargs))
        )


class CompleteReport(ReportBaseAPI, ReportsRequestHandler):
    def get_handler(self, request, *args, **kwargs):
        return self.get_complete_reports

    def get_complete_reports(self, request, *args, **kwargs):
        return Response(
            data_converter(
                self.aggregate_reports(
                    request, report=self.daily_reports(request)
                )
            )
        )
