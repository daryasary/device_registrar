# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-05-26 16:49
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0003_auto_20180526_1638'),
        ('services', '0003_service_title'),
    ]

    operations = [
        migrations.CreateModel(
            name='ServiceUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 30 characters or fewer starting with a letter. Letters, digits and underscore only.', max_length=32, validators=[django.core.validators.RegexValidator('^[a-zA-Z][a-zA-Z0-9_\\.]+$', 'Enter a valid username starting with a-z. This value may contain only letters, numbers and underscore characters.', 'invalid')], verbose_name='username')),
                ('email', models.EmailField(blank=True, max_length=254, null=True, unique=True, verbose_name='email')),
                ('phone_number', models.CharField(error_messages={'unique': 'A user with this mobile number already exists.'}, max_length=12, validators=[django.core.validators.RegexValidator('^989[0-3,9]\\d{8}$', 'Enter a valid mobile number.', 'invalid')], verbose_name='phone number')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('is_active', models.BooleanField(default=True, verbose_name='is_active')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='updated')),
                ('service', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='users', to='services.Service', verbose_name='service')),
            ],
            options={
                'verbose_name': 'Service user',
                'verbose_name_plural': 'Services users',
                'ordering': ('-created', '-updated'),
            },
        ),
        migrations.CreateModel(
            name='UserLogin',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('is_active', models.BooleanField(default=True, verbose_name='is active')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='updated')),
                ('device', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='user', to='devices.Device', verbose_name='device')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='device', to='services.ServiceUser', verbose_name='user')),
            ],
            options={
                'verbose_name': 'User login',
                'verbose_name_plural': 'Users login',
                'ordering': ('-created', '-updated'),
            },
        ),
        migrations.AlterUniqueTogether(
            name='serviceuser',
            unique_together=set([('email', 'service'), ('username', 'service'), ('phone_number', 'service')]),
        ),
    ]
