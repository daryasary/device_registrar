from rest_framework.decorators import detail_route
from rest_framework.response import Response

from services.api.generics import ServiceAPIViewResponse, \
    UserBaseViewSet
from services.api.permissions import DeviceIsRegistered
from .serializers import ServiceUserSerializer, ByUsernameSerializer, \
    ByEmailSerializer, ByPhoneNumberSerializer, ServiceListSerializer, \
    LoginUserSerializer
from ..models import Service
from services.utils import detect_service, detect_scope, check_authority
from utils.auth.backends import DeviceRegistrarAuthentication
from mongopanel.utils import save_object, retrieve_object, update_object, \
    object_list
from utils.format import paginate_list


class ServiceWideAPIView(ServiceAPIViewResponse):
    """This is main view of service handler API, all aspect of CRUD will be
        handled here.

        This API can be called with several URL parameters, listed below:

        URL Args (None of them are mandatory):
            service_id (int): Preferred service id
            scope_slug (string): Intended scope's slug
            object_id (string): Intended object's id

        Terms:
            * If service_id is not provided:
                ** User is authenticated: Return user's services list
                ** User is Not authenticated: raise unauthorized

            * If scope_slug is not provided: Return service's scope list

            * If object_id provided:
                ** If request method is GET: Return object
                ** If request method is PUT: Update object

            * If scope_slug is provided:
                ** if object_id not provided: Return paginated scope objects list

        """
    authentication_classes = (DeviceRegistrarAuthentication, )

    def get(self, request, service_id=None, scope_slug=None, object_id=None):
        if service_id is None:
            services = Service.objects.filter(user=request.user)
            serializer = ServiceListSerializer(services, many=True)
            return Response(serializer.data)

        service = detect_service(service_id)
        if scope_slug:
            scope = detect_scope(service, scope_slug)
        else:
            return self.service_scopes_list(service)

        if object_id:
            obj = retrieve_object(request, service, scope, object_id)
            return self.object_response(obj)
        else:
            # Return List of entered objects
            obj_list = object_list(request, service, scope)
            return self.object_response(
                paginate_list(request, *obj_list)
            )

    def post(self, request, service_id, scope_slug=None, object_id=None):
        service = detect_service(service_id)
        scope = detect_scope(service, scope_slug)
        check_authority(scope, raise_exception=True)
        inserted_objects = save_object(service, scope, request)
        return self.inserted_list(inserted_objects)

    def put(self, request, service_id, scope_slug=None, object_id=None, act='set'):
        service = detect_service(service_id)
        scope = detect_scope(service, scope_slug)
        updated_obj = update_object(
            request, service, scope, object_id, act
        )
        return self.updated_info(updated_obj)


class RegisterLoginViewSet(UserBaseViewSet):
    """Complete user registration API for specific services, This API
    needs authentication, device uuid in <DRG Token> format should be
    placed on request, header and user with provided credentials will
    create for given service.
    Users can be created in these three ways:
        - Create with username & password
        - Create with email (Google oauth)
        - Create with phone_number
    """
    authentication_classes = (DeviceRegistrarAuthentication, )
    permission_classes = (DeviceIsRegistered, )
    serializer_class = ServiceUserSerializer

    # Create user by passing username, password and email address of user
    @detail_route(methods=['post'])
    def by_username(self, request, service_id):
        serializer = ByUsernameSerializer(
            data=self.prepare_data(request, service_id)
        )
        return self.create_user(serializer)

    # Create user instance with OAUTH and connection to Google
    @detail_route(methods=['post'])
    def by_email(self, request, service_id):
        # TODO: Check routes and verifications flow
        serializer = ByEmailSerializer(
            data=self.prepare_data(request, service_id)
        )
        return self.create_user(serializer)

    # Create user with the given phone_number and verify it
    @detail_route(methods=['post'])
    def by_phone_number(self, request, service_id):
        # TODO: Send message and verifications
        serializer = ByPhoneNumberSerializer(
            data=self.prepare_data(request, service_id)
        )
        return self.create_user(serializer)


class LoginUserAPIView(UserBaseViewSet):
    authentication_classes = (DeviceRegistrarAuthentication,)
    permission_classes = (DeviceIsRegistered,)
    serializer_class = LoginUserSerializer

    def post(self, request, service_id, *args, **kwargs):
        # 1- Pass credentials to serializer
        # 2- Validate serializer
        # 3- Login user if credentials are true
        serializer = self.get_serializer(
            data=self.prepare_data(request, service_id)
        )
        serializer.is_valid(raise_exception=True)
        return Response("ok")

register_service_users_by_username = RegisterLoginViewSet.as_view(
    {"post": "by_username"}
)
register_service_users_by_email = RegisterLoginViewSet.as_view(
    {"post": "by_email"}
)
register_service_users_by_phone_number = RegisterLoginViewSet.as_view(
    {"post": "by_phone_number"}
)
authenticate_user_by_credentials = LoginUserAPIView.as_view({"post": "post"})
service_specific_view = ServiceWideAPIView.as_view()
