from django.utils.translation import ugettext_lazy as _

from rest_framework.permissions import BasePermission

from services.utils import authentication_required


class DeviceIsRegistered(BasePermission):
    """Check whether user device is not registered in Device model
    or not, if is registered should be place in request.user.device"""
    message = _("Device is not registered")

    def has_permission(self, request, view):
        return hasattr(request.user, 'device')
