from django.contrib.auth.hashers import make_password
from django.core.validators import validate_email
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from services.models import Service, Scope, ServiceUser
from services.utils import authenticate, login_user


class ServiceListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service
        fields = ('id', 'title', 'slug', 'package_name',
                  'package_version', 'created_time')


class ScopeListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Scope
        fields = '__all__'


class ByUsernameSerializer(serializers.ModelSerializer):
    """ByUsername class create user for specific services with given
    credentials, credentials can be (username, email, password) or
    (email, password)"""

    class Meta:
        model = ServiceUser
        fields = ("username", "email", "password", "service")
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        password = validated_data.pop("password")
        instance = ServiceUser.objects.create(**validated_data)
        instance.password = make_password(password)
        instance.save()
        return instance


class ByEmailSerializer(serializers.ModelSerializer):

    class Meta:
        model = ServiceUser
        fields = ("email", "service")


class ByPhoneNumberSerializer(serializers.ModelSerializer):
    """Register user by their provided phone number for the given service
    send verification code and verify phone_number should been controlled"""
    pass


class ServiceUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = ServiceUser
        fields = ("username",)

    def validate(self, attrs):
        raise ValidationError(_("Wrong method call"))


class LoginUserSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField(write_only=True)
    service = serializers.CharField()

    def validate(self, attrs):
        request = self.context['request']
        username = attrs.get('username')
        service = attrs.get('service')
        try:
            validate_email(username)
            user = ServiceUser.objects.get(email=username, service=service)
            username = False
        except ServiceUser.DoesNotExist:
            msg = _('Unable to login with provided credentials.')
            raise serializers.ValidationError(msg)
        except Exception:
            pass
        credentials = {
            'username': username,
            'password': attrs.get('password'),
            'service': service
        }

        if all(credentials.values()):
            user = authenticate(**credentials)
            if user:
                if not user.is_active:
                    msg = _('User account is disabled.')
                    raise serializers.ValidationError(msg)
                device = request.user.device
                user_device = login_user(user, device)
                return user_device
            else:
                msg = _('Unable to login with provided credentials.')
                raise serializers.ValidationError(msg)
        else:
            msg = _('Must include username/email and "password".')
            raise serializers.ValidationError(msg)
