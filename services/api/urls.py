from django.conf.urls import url, include

from services.api.views import service_specific_view, register_service_users_by_username, \
    register_service_users_by_email, register_service_users_by_phone_number, \
    authenticate_user_by_credentials

urlpatterns = [
    url(r'^$', service_specific_view, name="services"),
    url(r'^(?P<service_id>[0-9]+)/$', service_specific_view, name="scopes_list"),
    url(r'^(?P<service_id>[0-9]+)/auth/register/credentials/$', register_service_users_by_username, name="register_user_by_username"),
    url(r'^(?P<service_id>[0-9]+)/auth/register/oauth/$', register_service_users_by_email, name="register_user_by_email"),
    url(r'^(?P<service_id>[0-9]+)/auth/register/phone_number/$', register_service_users_by_phone_number, name="register_user_by_phone_number"),
    url(r'^(?P<service_id>[0-9]+)/auth/login/$', authenticate_user_by_credentials, name="register_user_by_phone_number"),
    url(r'^(?P<service_id>[0-9]+)/(?P<scope_slug>[\w-]+)/$', service_specific_view, name="scopes_data"),
    url(r'^(?P<service_id>[0-9]+)/(?P<scope_slug>[\w-]+)/(?P<object_id>[\w-]+)/$', service_specific_view, name="single_scope"),
]
