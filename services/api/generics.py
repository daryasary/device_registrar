from rest_framework import mixins
from rest_framework import status
from rest_framework.settings import api_settings

from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet
from rest_framework.response import Response


from services.api.serializers import ScopeListSerializer
from services.models import ServiceUser


class ServiceAPIViewResponse(APIView):
    @staticmethod
    def readonly_scope(scope):
        return Response(scope.default_response, status=status.HTTP_200_OK)

    @staticmethod
    def subscription_created():
        return Response(
            {"msg": "Subscription created successfully"},
            status=status.HTTP_201_CREATED
        )

    @staticmethod
    def subscription_exists():
        return Response(
            {"msg": "Subscription already exists"},
            status=status.HTTP_200_OK
        )

    @staticmethod
    def default_response():
        return Response(
            {"msg": "No suitable action found for your request"},
            status=status.HTTP_200_OK
        )

    @staticmethod
    def inserted_list(inserted_objects):
        return Response(
            {"objects_id_list": inserted_objects},
            status=status.HTTP_201_CREATED
        )

    @staticmethod
    def object_response(data):
        if data:
            return Response(data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_404_NOT_FOUND)

    @staticmethod
    def updated_info(dic):
        return Response(dic, status=status.HTTP_201_CREATED)

    @staticmethod
    def service_scopes_list(service):
        qs = service.scopes.all()
        serializer = ScopeListSerializer(qs, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class UserBaseViewSet(GenericViewSet):

    def create_user(self, serializer):
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    def prepare_data(self, request, service_id):
        request.data['service'] = service_id
        if "username" not in request.data:
            request.data['username'] = ServiceUser.generate_username(
                request.data["service"]
            )
        return request.data

    def perform_create(self, serializer):
        serializer.save()

    def get_success_headers(self, data):
        try:
            return {'Location': str(data[api_settings.URL_FIELD_NAME])}
        except (TypeError, KeyError):
            return {}
