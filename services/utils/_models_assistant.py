import string

from django.utils.crypto import random
from rest_framework.exceptions import NotFound, PermissionDenied
from django.utils.translation import ugettext_lazy as _

from services.models import Service, Scope, ServiceSubscriptions, ServiceUser, \
    UserLogin

subscribe_user = ServiceSubscriptions.subscribe
subscribe_check = ServiceSubscriptions.is_subscribed


def detect_service(service_id):
    try:
        service = Service.objects.get(id=service_id)
    except Service.DoesNotExist:
        raise NotFound(detail=_("Service not found"))
    return service


def detect_scope(service, scope_slug):
    try:
        scope = Scope.objects.select_related('service').get(
            service=service, slug=scope_slug
        )
    except Scope.DoesNotExist:
        raise NotFound(detail=_("Service not found"))
    return scope


def check_authority(scope, raise_exception=False):
    """ Check write is allowed in this scope or not, if raise_exception
    set to True will raise PermissionDenied exception otherwise return
    authority"""
    if scope.read_only:
        if raise_exception:
            raise PermissionDenied(
                detail=_('Post method is not allowed to the read only scopes')
            )
    return not scope.read_only


def authentication_required(service_id, scope_slug):
    scope = detect_scope(service_id, scope_slug)
    return scope.registration_required


def random_string(count=3):
    return ''.join(random.choice(string.digits) for _ in range(count))


def authenticate(username, password, service):
    try:
        user = ServiceUser.objects.get(username=username)
    except ServiceUser.DoesNotExist:
        return None
    else:
        if user.check_password(password) and user.check_service(service):
            return user


def login_user(user, device):
    login, created = UserLogin.object.create_or_update(
        device=device, defaults={'user': user}
    )
    return login