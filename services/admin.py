from django.contrib import admin

from .models import Service, ConstantResponse, Scope, ServiceUser


class ConstantResponseInline(admin.TabularInline):
    model = ConstantResponse
    extra = 1
    readonly_fields = ('object_id',)

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Scope)
class ScopeAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'slug', 'read_only', 'registration_required', 'service')
    inlines = (ConstantResponseInline,)


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    list_display = ('title', 'package_name', 'slug', 'package_version', 'user', 'token')
    list_filter = ('package_name', )
    prepopulated_fields = {"slug": ("title",)}

    def save_model(self, request, obj, form, change):
        if not hasattr(obj, 'user'):
            obj.user = request.user
        super(ServiceAdmin, self).save_model(request, obj, form, change)

    def token(self, obj):
        return obj.get_jwt_token()


@admin.register(ServiceUser)
class ServiceUserAdmin(admin.ModelAdmin):
    list_display = [
        'username', 'email', 'phone_number', 'service', 'is_active', 'created'
    ]
    list_filter = ['service']
    search_fields = ['username', 'email', 'phone_number', 'service']
    readonly_fields = ("password", "service")

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(service__in=request.user.service.values('id'))

    def has_add_permission(self, request):
        return False

admin.site.index_template = 'admin/custom_index.html'

