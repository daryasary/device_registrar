import json
import random
import string

from django.apps import apps
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import IntegrityError
from django.test import Client
from django.test import TestCase
from django.urls import reverse

from services.api.serializers import ServiceListSerializer
from services.apps import ServicesConfig
from services.models import Service, Scope


class ServiceAppAndConfigurationTest(TestCase):
        def test_service_name_and_translation(self):
            # TODO: Check translation names later
            self.assertEqual(ServicesConfig.name, 'services')
            self.assertEqual(apps.get_app_config('services').name, 'services')


class ServiceModelInitialTest(TestCase):
    @classmethod
    def load_user(cls):
        staff = {
            "username": "staff_test",
            "password": "QAZwsx123",
            "is_staff": True
        }
        try:
            staff_user = User.objects.get(username=staff['username'])
        except User.DoesNotExist:
            staff_user = User.objects.create_user(**staff)
        return staff_user

    @classmethod
    def setUpTestData(cls):
        staff = cls.load_user()
        service = Service.objects.create(name='teemcheh', moderator=staff)
        scope1_data = {"name": "config", "service": service,
                       "default_response": {}}
        scope2_data = {"name": "home", "service": service,
                       "default_response": {}}
        Scope.objects.create(**scope1_data)
        Scope.objects.create(**scope2_data)

    def setUp(self):
        self.service = Service.objects.first()
        self.scope1 = Scope.objects.first()
        self.scope2 = Scope.objects.last()

    def test_unique_slug_for_each_service(self):
        # Model will ignore services with similar slugs
        with self.assertRaises(IntegrityError):
            Service.objects.create(slug=self.service.slug)

    def test_scopes_validation_and_clean_method(self):
        # Check __str__ method of Scope model
        self.assertEquals(
            self.scope1.__str__(), '{}: {}'.format(
                self.scope1.service, self.scope1.name
            )
        )

        # Check that all scopes of a service should be unique
        with self.assertRaises(ValidationError):
            Scope.objects.create(name="config", service=self.service)

        # Check read_only scopes should have default response
        read_only_instance = Scope(
            name="config",
            service=self.service,
            read_only=True,
            default_response=''
        )
        self.assertRaises(ValidationError, read_only_instance.clean)


class ServiceRoutingAndURLsTest(TestCase):

    @classmethod
    def load_user(cls):
        staff = {
            "username": "staff_test",
            "password": "QAZwsx123",
            "is_staff": True
        }
        try:
            staff_user = User.objects.get(username=staff['username'])
        except User.DoesNotExist:
            staff_user = User.objects.create_user(**staff)
        return staff_user

    @classmethod
    def get_service(cls):
        try:
            service = Service.objects.get(slug='teemcheh')
        except Service.DoesNotExist:
            staff = cls.load_user()
            service = Service.objects.create(name='teemcheh', moderator=staff)

        if service.scopes.count() == 0:
            _ = Scope.objects.create(
                name="config", service=service, default_response=''
            )
        return service

    @staticmethod
    def random_string_generator(size=10):
        chars = string.ascii_lowercase + string.digits
        return ''.join(random.choice(chars) for _ in range(size))

    def setUp(self):
        self.client = Client()
        self.service = self.get_service()
        self.scope = self.service.scopes.first()
        self.random_id = self.random_string_generator()

    def test_routes_reverse(self):
        services = self.client.get((reverse('services')))
        self.assertEquals(services.status_code, 200)

        serializer = ServiceListSerializer(Service.objects.all(), many=True)
        self.assertListEqual(
            json.loads(services.content.decode('utf8')), serializer.data
        )

        scopes_list = self.client.get(
            reverse('scopes_list', kwargs={'service_id': self.service.id})
        )
        self.assertEquals(scopes_list.status_code, 200)

        scopes_list = self.client.get(
            reverse('scopes_list', kwargs={'service_id': 12})
        )
        self.assertEquals(scopes_list.status_code, 404)

        scopes_data = self.client.get(
            (reverse('scopes_data', kwargs={
                'service_id': self.service.id,
                'scope_slug': self.scope.slug
            })))
        self.assertEquals(scopes_data.status_code, 200)

        single_scope = self.client.get(
            reverse('single_scope', kwargs={
                'service_id': self.service.id,
                'scope_slug': self.scope.slug,
                'object_id': self.random_id

            }))
        self.assertEquals(single_scope.status_code, 200)


class ServiceAdminPanelTest(TestCase):

    def create_staff_user(self):
        self.username = "test_staff"
        self.password = User.objects.make_random_password()
        user, created = User.objects.get_or_create(username=self.username)
        user.set_password(self.password)
        user.is_staff = True
        user.is_superuser = True
        user.is_active = True
        user.save()
        self.user = user

    def setUp(self):
        self.create_staff_user()
        self.client = Client()
        self.client.login(username=self.username, password=self.password)

    def test_admin_pages_availability(self):
        admin_pages = [
            "/admin/",
            "/admin/services/",
            "/admin/services/service/",
            "/admin/services/service/add/",
            "/admin/services/scope/",
            "/admin/services/scope/add/",
        ]
        for page in admin_pages:
            resp = self.client.get(page)
            assert resp.status_code == 200
            assert b"<!DOCTYPE html" in resp.content


class CompleteServiceViewsTest(TestCase):
    fixtures = ['fixtures/users.json', 'services/fixtures/services.json']

    def setUp(self):
        self.staff = User.objects.get(username='hosein')
        self.service = Service.objects.first()
        self.scope_home = self.service.scopes.get(slug='home')
        self.scope_config = self.service.scopes.get(slug='config')
        self.auth_client = Client()
        # self.auth_client.force_login(self.staff)
        self.auth_client.login(username='hosein', paswword='QAZwsx123')
        self.anon_client = Client()

    def test_loading_fixtures(self):
        self.assertIsNotNone(self.service)
        self.assertIsNotNone(self.staff)
        self.assertTrue(self.service.scopes.exists)
        self.assertTrue(self.staff.is_staff)

    def test_service_view_permissions(self):
        response = self.client.get((reverse('services')))
        self.assertEquals(response.status_code, 200)

        response = self.auth_client.get(
                    (reverse('scopes_data', kwargs={
                        'service_id': self.service.id,
                        'scope_slug': self.scope_config.slug
                    })))
        self.assertEquals(response.status_code, 200)

        response = self.anon_client.get(
            (reverse('scopes_data', kwargs={
                'service_id': self.service.id,
                'scope_slug': self.scope_home.slug
            }))
        )
        self.assertEquals(response.status_code, 401)

        response = self.anon_client.get(
            (reverse('scopes_data', kwargs={
                'service_id': self.service.id,
                'scope_slug': 'wrong_slug'
            }))
        )
        self.assertEquals(response.status_code, 404)

        response = self.anon_client.get(
            (reverse('scopes_data', kwargs={
                'service_id': self.service.id,
                'scope_slug': self.scope_home.slug
            })))
        self.assertEquals(response.status_code, 401)
