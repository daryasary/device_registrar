from django.db.models.signals import post_save
from django.dispatch import receiver

from services.models import ConstantResponse
# from utils.mongo import save_defaults, update_defaults
from mongopanel.utils import save_defaults, update_defaults


@receiver(post_save, sender=ConstantResponse)
def save_constants_to_mongo(sender, instance, created, **kwargs):
    if created:
        saved_instance = save_defaults(instance)
        instance.object_id = saved_instance['_id']
        instance.entity = saved_instance
        # Disconnect signal to prevent loop on saving signal
        post_save.disconnect(save_constants_to_mongo, sender=sender)
        instance.save()
        post_save.connect(save_constants_to_mongo, sender=sender)

    elif instance._entity != instance.entity:
        # Check if entity changed update
        updated_instance = update_defaults(instance)
        instance.entity = updated_instance
        post_save.disconnect(save_constants_to_mongo, sender=sender)
        instance.save()
        post_save.connect(save_constants_to_mongo, sender=sender)
