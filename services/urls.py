from django.conf.urls import url

from services.views import ServicesViewSet, DailyReport, CompleteReport, \
    AggregateReport


services_create_list = ServicesViewSet.as_view({'get': 'list', "post": "create"})
service_retrieve = ServicesViewSet.as_view({'get': 'retrieve'})
service_token = ServicesViewSet.as_view({'post': 'get_token'})

urlpatterns = [
    # url(r'^$', ServicesListAPIView.as_view(), name='service_list'),
    url(r'^$', services_create_list, name='services_list'),
    url(r'^(?P<pk>[0-9]+)/$', service_retrieve, name='service_retrieve'),
    url(r'^(?P<pk>[0-9]+)/token/$', service_token, name='service_token'),
    url(r'^reports/complete/$', CompleteReport.as_view(), name='complete_reports'),
    url(r'^reports/daily/$', DailyReport.as_view(), name='daily_reports'),
    url(r'^reports/aggregate/$', AggregateReport.as_view(), name='aggregate_reports'),
]
