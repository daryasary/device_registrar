from django.contrib.auth.hashers import check_password
from django.contrib.postgres.fields import JSONField
from django.core import validators
from django.core.exceptions import ValidationError
from django.db import models
from django.conf import settings
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

from devices.models import Device
from utils.jwt_utils import jwt_encode_handler, jwt_payload_handler


class Service(models.Model):
    created_time = models.DateTimeField(_("Created time"), auto_now_add=True)
    updated_time = models.DateTimeField(_("Modified time"), auto_now=True)
    title = models.CharField(_("Title"), max_length=128)
    package_name = models.CharField(max_length=250)
    package_version = models.IntegerField()
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_("Moderator"),
        related_name="service", editable=False
    )
    slug = models.SlugField(
        verbose_name=_("Slug"), db_index=False,  # unique=True, Temporary disabled
        help_text=_("* This field will be generated automatically <br>"
                    "* Only change if generated slug matters you <br> "
                    "* Avoid to use space and upper case characters")
    )

    def __str__(self):
        return '%s v%s' % (self.package_name, self.package_version)

    class Meta:
        db_table = 'services'

    def get_payload(self):
        payload = {
            'user_id': self.user_id,
            'service_id': self.id,
        }
        return jwt_payload_handler(payload)

    def get_jwt_token(self):
        return jwt_encode_handler(self.get_payload())

    def save(self, **kwargs):
        # Make slug from name if it's empty, check if slug is repetitive
        from services.utils import random_string
        if not self.slug:
            self.slug = slugify(self.title)

        while Service.objects.filter(slug=self.slug).exclude(id=self.id).exists():
            self.slug = slugify(self.title) + random_string()

        super(Service, self).save(**kwargs)


class Scope(models.Model):
    created_time = models.DateTimeField(_("Created time"), auto_now_add=True)
    updated_time = models.DateTimeField(_("Modified time"), auto_now=True)
    name = models.CharField(_("Name"), max_length=32)
    slug = models.SlugField(_("Slug"))
    read_only = models.BooleanField(_("Read only"), default=False)
    registration_required = models.BooleanField(_("Registration required"), default=False)
    service = models.ForeignKey(Service, verbose_name=_("Service"), related_name="scopes")

    def __str__(self):
        return '{}: {}'.format(self.service, self.name)

    # TODO: This modification should move to admin
    # def clean(self):
    #     if self.read_only and not self.defaults.exists():
    #         raise ValidationError(
    #             _("Read only scopes must have default response")
    #         )

    def save(self, **kwargs):
        # Make slug from name if it's empty, check if slug is repetitive
        if not self.slug:
            self.slug = slugify(self.name)
        current_slugs = self.service.scopes.values_list(
            'slug', flat=True
        ).exclude(id=self.id)
        if self.slug in current_slugs:
            raise ValidationError(
                _("Slug: {} is already exists in {} service scopes".format(
                    self.slug, self.service
                ))
            )
        super(Scope, self).save(**kwargs)


class ConstantResponse(models.Model):
    object_id = models.CharField(max_length=128, blank=True)
    scope = models.ForeignKey(Scope, related_name='defaults')
    entity = JSONField()

    def __init__(self, *args, **kwargs):
        super(ConstantResponse, self).__init__(*args, **kwargs)
        self._entity = self.entity

    def __str__(self):
        return self.object_id

    @property
    def user(self):
        return self.scope.service.user


class ServiceSubscriptions(models.Model):
    service = models.ForeignKey(Service, related_name='subscriptions')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='subscriptions')

    def __str__(self):
        return '{} : {}'.format(self.service, self.user)

    @classmethod
    def subscribe(cls, service, user):
        instance, result = cls.objects.get_or_create(service=service, user=user)
        return result

    @classmethod
    def is_subscribed(cls, service, user):
        return cls.objects.filter(service=service, user=user).exists()


class ServiceUser(models.Model):
    username = models.CharField(
        _('username'), max_length=32, null=True, blank=True,
        help_text=_(
            'Required. 30 characters or fewer starting with a letter. '
            'Letters, digits and underscore only.'
        ),
        validators=[validators.RegexValidator(
            r'^[a-zA-Z][a-zA-Z0-9_\.]+$',
            _('Enter a valid username starting '
              'with a-z. This value may contain '
              'only letters, numbers and '
              'underscore characters.'), 'invalid'),],
        error_messages={
            'unique': _("A user with that username already exists."),
        }
    )
    email = models.EmailField(_("email"), null=True, blank=True)
    phone_number = models.CharField(
        _("phone number"),
        max_length=12, null=True, blank=True,
        validators=[
            validators.RegexValidator(r'^989[0-3,9]\d{8}$',
                                      _('Enter a valid mobile number.'),
                                      'invalid'),
        ],
        error_messages={
            'unique': _("A user with this mobile number already exists."),
        }
    )
    password = models.CharField(_('password'), max_length=128)
    service = models.ForeignKey(
        Service, verbose_name=_('service'), related_name='users'
    )
    is_active = models.BooleanField(_("is_active"), default=True)
    created = models.DateTimeField(_("created"), auto_now_add=True)
    updated = models.DateTimeField(_("updated"), auto_now=True)

    class Meta:
        verbose_name = _("Service user")
        verbose_name_plural = _("Services users")
        unique_together = (
            ("username", "service"),
            ("email", "service"),
            ("phone_number", "service"),
        )
        ordering = ("-created", "-updated")

    def __str__(self):
        return "{} > {}".format(self.username, self.service)

    @classmethod
    def generate_username(cls, service_id):
        from services.utils import random_string
        _username = 'user_{}{}'.format(service_id, random_string(4))
        while cls.objects.filter(username=_username, service_id=service_id).exists():
            _username = 'user_{}{}'.format(service_id, random_string(4))
        return _username

    def check_password(self, raw_password):
        return check_password(raw_password, self.password)

    def check_service(self, service_id):
        return int(service_id) == int(self.service.id)

    def can_authenticate(self):
        return self.is_active


class UserLogin(models.Model):
    user = models.ForeignKey(
        ServiceUser, related_name='device', verbose_name=_("user")
    )
    device = models.OneToOneField(
        Device, related_name='user', verbose_name=_("device")
    )
    is_active = models.BooleanField(_("is active"), default=True)
    created = models.DateTimeField(_("created"), auto_now_add=True)
    updated = models.DateTimeField(_("updated"), auto_now=True)

    class Meta:
        verbose_name = _("User login")
        verbose_name_plural = _("Users login")
        ordering = ("-created", "-updated")

    def __str__(self):
        return "{} > {}".format(self.user, self.device)

    @classmethod
    def filter(cls, service):
        return cls.objects.filter(service=service)
