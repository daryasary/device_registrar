For migrate database to a new version:

`python manage.py dumpdata --format=json devices> devices_dump.json
`

Import auth_users & services table's data manually and then:

`python manage.py loaddata --app=devices devices_dump.json  `

**local_settings template**

    BASE_URL = base url of project
    ALLOWED_HOSTS = list of allowed hosts

    DEBUG = boolean for debugging mode
    DEVEL = boolean for developing mode

    DB_NAME = name of database
    DB_USER = username of database
    DB_PASS = password of database
    DB_HOST = host of database


    RBMQ_URL = url of rabbitMQueue
    RBMQ_ROUTE_KEY = route key of rabbitMQueue

    CACHE_LOCATION = host of Mem Cache
    CACHE_TIMEOUT = cache timeout

    MAX_LOG_COUNT = number of objects per bulk insert
    MAX_LOG_TIME = maximum time of each consumer
    MAX_TIME_INACTIVE = desc needed

    MAX_TIME_WAIT = maximum waiting time

    MONGO_URI = Mongo db URI
    MONGO_HOST = Mongo db Host
    MONGO_PORT = '27017'
    MONGO_NAME = Mongo db name

**notes**

in unit tests, clear rabbitMQueue before run the tests
