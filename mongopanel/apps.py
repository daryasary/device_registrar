from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class MongopanelConfig(AppConfig):
    name = 'mongopanel'
    verbose_name = _('Mongo Panel')
