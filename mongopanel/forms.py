from django import forms


class MongoObjectForm(forms.Form):
    data = forms.CharField(widget=forms.Textarea)
    # data = forms.CharField(widget=JSONEditor)
    # data = JSONFormField()
