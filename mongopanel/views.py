import json

from django.conf import settings
from django.contrib.admin.options import IS_POPUP_VAR
from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View
from django.utils.translation import ugettext_lazy as _

from mongopanel.forms import MongoObjectForm
from services.models import Service
from utils.format import bson2dict
from mongopanel.utils import scopes_name_list, scope_objects_list, \
    retrieve_admin_object, update_admin, delete_admin


admin_decorators = [login_required, user_passes_test(lambda u: u.is_superuser)]


@method_decorator(admin_decorators, name='dispatch')
class MongoPanel(View):
    def get(self, request, *args, **kwargs):
        if 'service' in kwargs:
            # filter() used instead of get() because queryset is needed
            # in following of code
            services = Service.objects.filter(
                user=request.user, slug=kwargs.get('service')
            )
        else:
            services = Service.objects.filter(user=request.user)

        context = dict(
            collections=scopes_name_list(
                services.values_list('slug', flat=True)),
            services=services,
        )
        return render(
            request, "admin/collections_change_list.html", context=context
        )


@method_decorator(admin_decorators, name='dispatch')
class MongoCollectionPanel(View):
    def paginate_response(self, request, obj_list, obj_count):
        if 'p' in request.GET:
            page = int(request.GET.get('p'))
            if obj_count > settings.PAGE_SIZE:
                result = obj_list[
                         settings.PAGE_SIZE * page:settings.PAGE_SIZE * (
                             page + 1)]
            else:
                result = obj_list[:settings.PAGE_SIZE]
        else:
            result = obj_list[:settings.PAGE_SIZE]
            page = None

        return result, {
            'total_objects': obj_count,
            'current': page,
            'total_pages': obj_count // settings.PAGE_SIZE
        }

    def get(self, request, collection, *args, **kwargs):
        if 'object_id' in kwargs:
            obj = retrieve_admin_object(collection, kwargs.get('object_id'))
            adminForm = MongoObjectForm({'data': json.dumps(bson2dict(obj))})
            opts = dict(app_label=collection, model_name='Objects')
            app_label = opts['app_label']
            context = dict(
                title=(_('Update object')),
                adminform=adminForm,
                object_id=str(obj['_id']),
                original=obj,
                collection=collection,
                is_popup=(IS_POPUP_VAR in request.POST or
                          IS_POPUP_VAR in request.GET),
                has_add_permission=False,
                has_change_permission=False,
                has_delete_permission=False,
                has_file_field=False,
                form_url="",
                opts=opts,
                app_label=app_label,
                change=True,
                save_as=False
            )

            request.current_app = 'Mongo'

            return render(request, "admin/change_object_form.html", context)

        # Load paginated list of objects
        results_data = self.paginate_response(
            request, *scope_objects_list(collection)
        )
        context = dict(
            user=request.user,
            collection=collection,
            results=results_data[0],
            result_headers=['id', 'device', 'insert_time', 'data'],
            pagination_result=results_data[1]
        )
        return render(
            request, "admin/collections_object_page.html", context=context
        )

    def post(self, request, collection, object_id, *args, **kwargs):
        data = MongoObjectForm(request.POST)
        data.is_valid()
        result = update_admin(collection, object_id, data.cleaned_data['data'])
        return redirect(reverse('collection_objects', args=[collection]))


@method_decorator(admin_decorators, name='dispatch')
class MongoObjectDelete(View):
    def get(self, request, collection, object_id, *args, **kwargs):
        result = delete_admin(collection, object_id)
        return redirect(reverse('collection_objects', args=[collection]))
