from django.conf.urls import url

from mongopanel.views import MongoPanel, MongoCollectionPanel, MongoObjectDelete

urlpatterns = [
    url(r'^$', MongoPanel.as_view(), name="mongo_collections"),
    url(r'service/(?P<service>[\w-]+)/$', MongoPanel.as_view(), name="service_collections"),
    url(r'collection/(?P<collection>[\w-]+)/$', MongoCollectionPanel.as_view(), name="collection_objects"),
    url(r'collection/(?P<collection>[\w-]+)/(?P<object_id>[\w-]+)/$', MongoCollectionPanel.as_view(), name="collection_object"),
    url(r'collection/(?P<collection>[\w-]+)/(?P<object_id>[\w-]+)/delete$', MongoObjectDelete.as_view(), name="delete_object"),
]
