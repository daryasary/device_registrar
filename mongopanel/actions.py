import json
from datetime import datetime

from django.conf import settings
from pymongo import MongoClient

from utils.format import clean_time


def init_mongo(**kwargs):
    # TODO: Check if custom mongo configurations passed to function
    # client = MongoClient(settings.MONGO_CONFIGURATION)
    # mongo_db = client[settings.MONGO_DB]

    connection = MongoClient(settings.MONGO_HOST)
    mongo_db = connection[settings.MONGO_DB]
    mongo_db.authenticate(settings.MONGO_USER, settings.MONGO_PASS)
    return mongo_db


def make_collection_name(service, scope):
    return '{}_{}'.format(service.slug, scope.slug)


def generate_insert_data(data, _user=None, _device_uuid=None, action_time=datetime.now()):
    if '_user' not in data.keys():
        data.update({'_user': _user})
    if '_device_uuid' not in data.keys():
        data.update({'_device_uuid': _device_uuid})
    if '_insert_time' not in data.keys():
        data.update({'_insert_time': clean_time(action_time)})
    data.update({'_modify_time': clean_time(action_time)})
    data.pop('_id', None)
    return data


def generate_update_data(data, action_time=datetime.now()):
    if not isinstance(data, dict):
        data = json.loads(data)
    for key in ['_id', '_user', '_device_uuid', '_insert_time']:
        data.pop(key, None)
    data.update({'_modify_time': clean_time(action_time)})
    return data
