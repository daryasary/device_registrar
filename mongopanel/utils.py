from bson import ObjectId

from pymongo.errors import BulkWriteError, DuplicateKeyError

from mongopanel.actions import init_mongo, generate_insert_data, \
    generate_update_data, make_collection_name
from services.models import ConstantResponse
from utils.format import bson2dict, filter_by_service
from utils.format import is_constant


class BaseMongoHandler:
    def __init__(self, **kwargs):
        self.db = init_mongo(**kwargs)

    def get_intended_scope(self, collection):
        return getattr(self.db, collection)

    def get_collections_list(self):
        return self.db.collection_names()

    def get_objects_list(self, collection, query={}, sort=None, limit=0):
        intended = self.get_intended_scope(collection)
        if isinstance(query, dict):
            query = [query]

        if sort is None:
            result = intended.find(*query).limit(limit)

        else:
            result = intended.find(*query).sort(sort).limit(limit)

        return result, result.count()

    def get_object(self, collection, query={}):
        intended = self.get_intended_scope(collection)
        if isinstance(query, dict):
            query = [query]
        return bson2dict(intended.find_one(*query))

    def save_object(self, collection, data):
        intended = self.get_intended_scope(collection)
        inserted = intended.insert_one(data)
        return bson2dict(intended.find_one({'_id': inserted.inserted_id}))

    def delete_object(self, collection, query):
        data = self.get_object(collection, query)
        if is_constant(data):
            response = ConstantResponse.objects.get(object_id=data['_id'])
            response.delete()
        intended = self.get_intended_scope(collection)
        return intended.delete_one(query)

    def save_list(self, collection, data):
        intended = self.get_intended_scope(collection)

        try:
            inserted = intended.insert_many(data)
            ids = [str(i) for i in inserted.inserted_ids]
        except BulkWriteError:
            # This error means there is an error inside a list and
            # objects should been saved separately one by one
            ids = list()
            for d in data:
                try:
                    ids.append(str(intended.insert_one(d).inserted_id))
                except DuplicateKeyError:
                    # This dict is previously saved so continue for loop
                    ids.append(str(intended.find_one(d)['_id']))
        return ids

    def update_list(self, collection, query, data):
        intended = self.get_intended_scope(collection)
        updated = intended.update_many(query, data)
        return {
            "matched_count": updated.matched_count,
            "modified_count": updated.modified_count
        }


class MongoHandler(BaseMongoHandler):
    def get_services_collections_list(self, services_list=None):
        collections = self.get_collections_list()
        if services_list is not None:
            collections = filter_by_service(collections, services_list)
        return collections

    def retrieve_admin_object_from_mongo(self, collection, object_id):
        return self.get_object(collection, {'_id': ObjectId(object_id)})

    def get_admin_objects_list(self, collection):
        return self.get_objects_list(collection)

    def save_default_responses_to_mongo(self, constant):
        return self.save_object(
            make_collection_name(constant.scope.service, constant.scope),
            generate_insert_data(
                constant.entity, _user=constant.user.id, _device_uuid=None
            )
        )

    def update_default_responses_to_mongo(self, constant):
        updated = self.update_list(
            make_collection_name(constant.scope.service, constant.scope),
            {'_id': ObjectId(constant.object_id), '_device_uuid': None},
            {'$set': generate_update_data(constant.entity)}
        )
        return self.get_object(
            make_collection_name(constant.scope.service, constant.scope),
            {'_id': ObjectId(constant.object_id)}
        )

    def update_object_from_django_admin(self, collection, object_id, data):
        return self.update_list(
            collection,
            {'_id': ObjectId(object_id)},
            {'$set': generate_update_data(data)}
        )

    def delete_object_from_django_admin(self, collection, object_id):
        return self.delete_object(collection, {'_id': ObjectId(object_id)})

    def save_object_to_mongo(self, service, scope, request):
        data = request.data
        if not isinstance(data, list):
            # Handle one object or a bunch of objects for saving to mongo
            data = [data, ]
        data = map(
            lambda i: generate_insert_data(
                i, _user=request.user.id, _device_uuid=str(request.user.device)
            ) or i, data
        )
        return self.save_list(make_collection_name(service, scope), data)

    def retrieve_object_from_mongo(self, request, service, scope, object_id):
        query = {
            '_id': ObjectId(object_id),
            '$or': [{'_user': str(request.user.id)}, {'_device_uuid': None}]
        }
        return self.get_object(make_collection_name(service, scope), query)

    def get_list_objects_from_mongo(self, request, service, scope):
        return self.get_objects_list(
            make_collection_name(service, scope),
            {'$or': [
                {'_user': str(request.user.id)},
                {'_device_uuid': None}
            ]}
        )

    def update_object_in_mongo(self, request, service, scope, object_id, act):
        data = request.data
        return self.update_list(
            make_collection_name(service, scope),
            {'_id': ObjectId(object_id), '_device_uuid': str(request.user.device)},
            {'${}'.format(act): data}
        )

# API Side modules
save_object = MongoHandler().save_object_to_mongo
retrieve_object = MongoHandler().retrieve_object_from_mongo
object_list = MongoHandler().get_list_objects_from_mongo
update_object = MongoHandler().update_object_in_mongo

# Signal side modules
save_defaults = MongoHandler().save_default_responses_to_mongo
update_defaults = MongoHandler().update_default_responses_to_mongo

# Admin panel side modules
scopes_name_list = MongoHandler().get_services_collections_list
retrieve_admin_object = MongoHandler().retrieve_admin_object_from_mongo
scope_objects_list = MongoHandler().get_admin_objects_list
update_admin = MongoHandler().update_object_from_django_admin
delete_admin = MongoHandler().delete_object_from_django_admin
