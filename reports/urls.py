from django.conf.urls import url
from django.views.decorators.cache import cache_page

from .views import daily_report_registrar_devices, \
    aggregation_report_registrar_devices

urlpatterns = [
    # TODO: Remember to enable cache in future cache_page(60 * 60 * 4)
    url(r'^daily/$', daily_report_registrar_devices, name='daily_report_registrar_devices'),
    url(r'^aggregation/$', aggregation_report_registrar_devices, name='service_aggregation_report'),
]
