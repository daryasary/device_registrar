from datetime import timedelta

from celery.schedules import crontab
from celery.task import periodic_task
from celery.utils.log import get_task_logger
from django.contrib.auth import get_user_model

from django.utils import timezone

from reports.utils.actions import calc_user_statistics

logger = get_task_logger(__name__)
User = get_user_model()


@periodic_task(name='Calculate stats', run_every=crontab(hour=0, minute=30))
def calc_statistics(day_before=0):
    day_diff = dict(hour=0, minute=0, second=0, microsecond=0)
    target_date = timezone.now().replace(**day_diff) - timedelta(days=day_before)
    logger.info("Calculating report for {}".format(target_date))

    users = User.objects.all()
    for user in users:
        calc_user_statistics(target_date, user)
