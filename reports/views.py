from datetime import datetime, timedelta

from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication

from reports.utils.query import mongo_query
from services.models import Service

from .reports_patterns_generator import generate_report_pattern, \
    filter_pattern, chart_pattern, pie_pattern


@api_view(['GET', 'POST'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def daily_report_registrar_devices(request):

    # Step-1: Populate filters
    from_date = request.data.get('start_date', None)
    to_date = request.data.get('end_date', None)

    filter_data = dict()
    if from_date:
        from_date = datetime.strptime(from_date, '%Y-%m-%d')
    else:
        day_diff = dict(hour=0, minute=0, second=0, microsecond=0)
        from_date = (timezone.now() - timedelta(days=30)).replace(**day_diff)

    filter_data['date'] = dict()
    filter_data['date']['$gte'] = from_date
    if to_date:
        to_date = datetime.strptime(to_date, '%Y-%m-%d')
        if 'date' not in filter_data.keys():
            filter_data['date'] = dict()
        filter_data['date']['$lt'] = to_date

    service_id = request.data.get('service_id', 0)
    # if service_id:
    #     get_object_or_404(Service, **{'id': service_id, 'user': request.user})

    filter_data.update(dict(service=service_id, user_id=request.user.id))
    values = {'data': 1, '_id': 0}
    # Step-2: Retrieve data
    statics = mongo_query(
        'registrar_daily_stats',
        [filter_data, values]
    )

    # Step-3: Create report templates
    services = Service.objects.filter(
        user=request.user
    ).values('id', 'package_name')
    services_filters = [
        {'text': srv['package_name'], 'value': srv['id']} for srv in services
        ]

    report = generate_report_pattern(
        'Device Registrar Daily Report',
        'device_registrar_daily_report'
    )
    report['filters'].append(filter_pattern(
        'date', 'date_georgia', ['start_date', 'end_date'])
    )
    report['filters'].append(filter_pattern(
        _('Service id'), 'dropdown', services_filters)
    )
    devices_numbers_daily_stacked_chart = chart_pattern(
        'Daily Devices number for Services',
        'chart-linear', 'Dates', 'Devices Number'
    )

    devices_numbers_daily_stacked_chart['graph_data']['data'] = statics

    report['data'].append(devices_numbers_daily_stacked_chart)

    return Response(report)


@api_view(['GET', 'POST'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def aggregation_report_registrar_devices(request):

    # Step-1: Populate filters
    service_id = request.data.get('service_id', 0)
    filter_data = dict(service=service_id, user_id=request.user.id)
    sort_key = [('date', -1)]

    # Step-2: Retrieve data
    statics = mongo_query(
        'registrar_aggregate_stats', filter_data, sort=sort_key, limit=1
    )
    if len(statics):
        statics = statics[0]

    # Step-3: Create report templates
    services = Service.objects.values('id', 'package_name')
    services_filters = [
        {'text': srv['package_name'], 'value': srv['id']} for srv in services
        ]

    report = generate_report_pattern(
        'Device Registrar Daily Report',
        'device_registrar_daily_report'
    )
    report['filters'].append(filter_pattern(
        _('service_id'), 'dropdown', services_filters)
    )

    sdk_versions_bar_chart = chart_pattern(
        'SDK versions chart', 'chart-basic-bar', 'SDK Versions', 'Y label'
    )

    sdk_versions_bar_chart['graph_data']['data'] = statics.get('sdk_versions')

    report['data'].append(sdk_versions_bar_chart)

    app_versions_bar_chart = chart_pattern(
        'APP versions chart', 'chart-basic-bar', 'App Versions', 'Y label'
    )

    app_versions_bar_chart['graph_data']['data'] = statics.get('app_versions')

    report['data'].append(app_versions_bar_chart)

    client_os_bar_chart = chart_pattern(
        'Client os chart', 'chart-basic-bar', 'OS Name', 'Count'
    )

    client_os_bar_chart['graph_data']['data'] = statics.get('os_stats')

    report['data'].append(client_os_bar_chart)

    client_models_bar_chart = chart_pattern(
        'Client models chart', 'chart-basic-bar', 'Device Model', 'Count'
    )

    client_models_bar_chart['graph_data']['data'] = statics.get('models_stats')

    report['data'].append(client_models_bar_chart)

    display_sizes_bar_chart = chart_pattern(
        'Client display size chart', 'chart-basic-bar', 'Display sizes',
    )

    display_sizes_bar_chart['graph_data']['data'] = statics.get('display_stats')

    report['data'].append(display_sizes_bar_chart)

    carriers_bar_chart = chart_pattern(
        'Carriers chart', 'chart-basic-bar', 'Display sizes', 'Count'
    )

    carriers_bar_chart['graph_data']['data'] = statics.get('carriers_stats')

    report['data'].append(carriers_bar_chart)

    languages_bar_chart = pie_pattern('Languages pie', 'pie', 'lang-pie')

    languages_bar_chart['graph_data']['data'] = statics.get('lang_stats')

    report['data'].append(languages_bar_chart)

    return Response(report)
