from mongopanel.utils import BaseMongoHandler


def save_to_mongo(context_daily=None, context_aggregate=None):
    mongo = BaseMongoHandler()
    ast, dst = None, None
    if context_daily:
        dst = mongo.save_object('registrar_daily_stats', context_daily)
    if context_aggregate:
        ast = mongo.save_object('registrar_aggregate_stats', context_aggregate)
    return ast, dst
