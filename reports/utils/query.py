from django.contrib.postgres.fields.jsonb import KeyTextTransform
from django.db.models import Count

from mongopanel.utils import BaseMongoHandler

from devices.models import DeviceLog, DeviceMembership
from reports.utils.format import clean_data


def extract_from_json_field(model, dict_data, json_field,
                            count_data, filter_data=None):
    if filter_data:
        queryset = model.objects.filter(
            **filter_data
        ).annotate(
            **dict_data
        ).values(
            json_field
        ).annotate(**count_data)
    else:
        queryset = model.objects.annotate(
            **dict_data
        ).values(
            json_field
        ).annotate(**count_data)
    return queryset


def extract_device_membership_extra(key, count_key='device__generated_uuid', limit=0,
                                    order_asc=False, until=None, service=None):
    dict_data = {key: KeyTextTransform(key, 'extra_data')}
    count_data = {'device_count': Count(count_key, distinct=True)}

    filter_data = dict()
    if service:
        if isinstance(service, list):
            filter_data['service__in'] = service
        else:
            filter_data['service'] = service

    if until:
        filter_data['created_time__lte'] = until

    if filter_data:
        queryset = extract_from_json_field(
            DeviceMembership, dict_data, key, count_data, filter_data
        )
    else:
        queryset = extract_from_json_field(
            DeviceMembership, dict_data, key, count_data
        )

    queryset = queryset.order_by('-device_count')

    if order_asc:
        queryset = queryset.order_by('device_count')

    if limit and queryset.count() > limit:
        queryset = queryset[:limit]

    return queryset


def extract_log_event(key, count_key='device_uuid'):
    dict_data = {key: KeyTextTransform(key, 'event_data')}
    count_data = {'device_count': Count(count_key, distinct=True)}
    return extract_from_json_field(DeviceLog, dict_data, key, count_data)


def mongo_query(collection, query, **kwargs):
    mongo = BaseMongoHandler()
    response, count = mongo.get_objects_list(collection, query, **kwargs)
    return clean_data(response)
