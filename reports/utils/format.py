def generate_bar_data(data, name_key, value_key, category_key=None):
    # TODO: Handle 'others' on limited charts
    return [{
                'name': str(d[name_key]) or "0",
                'value': d[value_key],
                'category': i + 1,
            } for i, d in enumerate(data)]


def generate_pie_data(data, name_key, value_key, limit=0):
    res = []
    others = 0
    if limit:
        for i, d in enumerate(data):
            if i < limit:
                res.append({
                    'name': str(d[name_key]) or "0",
                    'y': d[value_key],
                })
            else:
                others += d[value_key]
        res.append({
            'name': "Others",
            'y': others,
        })
    else:
        for d in data:
            res.append({
                'name': str(d[name_key]) or "0",
                'y': d[value_key],
            })
    return res


def clean_data(data_list):
    """clean_data function get a list of dictionaries which contains
    separate data for each date, in different categories iterate over
    each day and return single list of all reports.
      -input: [{'data': [{...1}, {...2}]}, {'data': [{...3}, {...4}]}]
      -output: [{...1}, {...2}, {...3}, {...4}] for list data
      -output: [key: [{...1}, {...2}, {...3}, {...4}]] for dict data"""

    cleaned_data = list()
    for i in data_list:
        data = i['data']
        if isinstance(data, dict):
            cleaned_data.append(i['data'])
        if isinstance(data, list):
            cleaned_data.extend(i['data'])
    return cleaned_data


def unify_carriers(data):
    """Mobile carries have different code-names and this will drive to
    make wrong chart for end user, so here we unify similar carriers"""

    mci = ['IR-MCI', 'IR-TCI', 'IR MCI', 'MCI']
    rightel = ['Rightel', 'RIGHTEL', 'RighTel']
    irancell = ['Irancell', 'MTN Irancell']

    MCI = {'name': 'MCI', 'value': 0, 'category': 1}
    MTN = {'name': 'MTN', 'value': 0, 'category': 2}
    RGT = {'name': 'Rightel', 'value': 0, 'category': 3}

    carriers = list()
    for i in data:
        if i['name'] in mci:
            MCI['value'] += i['value']
        elif i['name'] in irancell:
            MTN['value'] += i['value']
        elif i['name'] in rightel:
            RGT['value'] += i['value']
        else:
            carriers.append(i)

    # TODO: Sort categories by value dsc
    carriers.extend([MCI, MTN, RGT])
    return carriers
