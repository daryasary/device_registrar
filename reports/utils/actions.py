from django.db.models import Count
from datetime import timedelta

from devices.models import DeviceMembership, DeviceLog, Device

from reports.utils.format import generate_bar_data, generate_pie_data
from reports.utils.mongo import save_to_mongo
from reports.utils.query import extract_device_membership_extra
from services.models import Service


def calc_user_statistics(target_date, user):
    """Calculate process parent that get a specific user and
    retrieve stats for given user then store it to the mongo"""
    context_daily = dict(date=target_date, user_id=user.id, service=0)
    context_aggregate = dict(date=target_date, user_id=user.id, service=0)

    services = Service.objects.filter(user=user).values_list('id', flat=True)

    context_daily.update(dict(
        data=calc_daily_stats(target_date, list(services))
    ))
    context_aggregate.update(dict(
        data=calc_aggregate_stats(target_date, list(services))
    ))
    save_to_mongo(context_daily, context_aggregate)

    for service in services:
        context_daily = dict(
            date=target_date - timedelta(days=1), user_id=user.id,
            service=service, data=calc_daily_stats(target_date, service)
        )
        context_aggregate = dict(
            date=target_date - timedelta(days=1), user_id=user.id,
            service=service, data=calc_aggregate_stats(target_date, service)
        )
        save_to_mongo(context_daily, context_aggregate)


def calc_daily_stats(target_date, service=None):
    """ Calculate daily stats for services on the given target_date,
    daily_stats module create multiple Y points for only One X on charts
    if service is not given stats will be calculated for all user specific
    services instead """
    filter_data = dict(
                created_time__gte=target_date - timedelta(days=1),
                created_time__lt=target_date
    )
    if service:
        if isinstance(service, list):
            filter_data['service__in'] = service
        else:
            filter_data['service'] = service

    target_date_devices = DeviceMembership.objects.extra(
        {'created_date': "date(created_time)"}
    ).select_related(
        'device'
    ).filter(
        **filter_data
    ).values_list('device__generated_uuid', flat=True)

    target_date_devices_count = target_date_devices.count()
    target_date_returns_count = DeviceLog.objects.filter(
        **filter_data
    ).exclude(
        device_uuid__in=target_date_devices
    ).distinct('device_uuid').count()
    # This query get all distinct devices from DeviceLog table for which
    # Devices came from 'target_date_devices', this query may return values
    # different from which you expect :)
    # target_date_actives_count = DeviceLog.objects.filter(
    #     **filter_data
    # ).filter(
    #     device_uuid__in=target_date_devices
    # ).distinct('device_uuid').count()
    target_date_actives_count = target_date_devices_count + target_date_returns_count

    data = [
        {
            "category": str(target_date.date() - timedelta(days=1)),
            "name": "registered devices",
            "value": target_date_devices_count
        },
        {
            "category": str(target_date.date() - timedelta(days=1)),
            "name": "return users",
            "value": target_date_returns_count
        },
        {
            "category": str(target_date.date() - timedelta(days=1)),
            "name": "active users",
            "value": target_date_actives_count
        },
    ]
    return data


def calc_aggregate_stats(target_date, service=None):
    """ Calculate aggregate stats for given service(0 is all services)
    until given target_date.
    This will create multiple Y and multiple X points on chart. """
    context = dict()
    filter_data = {'created_time__lte': target_date}

    # SDK Versions chart
    sdk_versions = extract_device_membership_extra(
        'sdk_ver', until=target_date, service=service
    )
    bar_sdk_versions = generate_bar_data(
        sdk_versions, 'sdk_ver', 'device_count'
    )
    context['sdk_versions'] = bar_sdk_versions

    # APP Version chart
    app_versions = extract_device_membership_extra(
        'app_ver', until=target_date, service=service
    )
    bar_app_versions = generate_bar_data(
        app_versions, 'app_ver', 'device_count'
    )
    context['app_versions'] = bar_app_versions

    # Display Size Chart
    display_sizes = extract_device_membership_extra(
        'display_size', limit=10, until=target_date, service=service
    )
    bar_display_size = generate_bar_data(
        display_sizes, 'display_size', 'device_count'
    )
    context['display_stats'] = bar_display_size

    # Carriers data
    carriers = extract_device_membership_extra(
        'carrier', limit=10, until=target_date, service=service
    )
    bar_carriers = generate_bar_data(carriers, 'carrier', 'device_count')
    context['carriers_stats'] = bar_carriers

    # Languages chart
    languages = extract_device_membership_extra(
        'lang', until=target_date, service=service
    )
    pi_languages = generate_pie_data(languages, 'lang', 'device_count', limit=2)
    context['lang_stats'] = pi_languages

    # Client OS chart
    if service:
        if isinstance(service, list):
            filter_data['service__in'] = service
        else:
            filter_data['service'] = service

    devices = DeviceMembership.objects.filter(
        **filter_data
    ).values_list('device_id', flat=True)

    client_os = Device.objects.filter(
        generated_uuid__in=devices
    ).values(
        'device_os__name'
    ).annotate(
        os_count=Count('generated_uuid')
    )

    client_os_versions = generate_bar_data(
        client_os.order_by('-os_count'), 'device_os__name', 'os_count'
    )
    context['os_stats'] = client_os_versions

    # Client models chart
    client_models = Device.objects.filter(
        generated_uuid__in=devices
    ).values(
        'device_model__name'
    ).annotate(
        model_count=Count('generated_uuid')
    )
    client_models_versions = generate_bar_data(
        client_models.order_by('-model_count')[:15],
        'device_model__name', 'model_count',
    )
    context['models_stats'] = client_models_versions
    return context
