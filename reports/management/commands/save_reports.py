from django.core.management.base import BaseCommand, CommandError

from reports.tasks import calc_statistics


class Command(BaseCommand):
    help = "Disable deleted tones"

    def add_arguments(self, parser):
        parser.add_argument('days', nargs='+', type=int)

    def handle(self, *args, **options):
        if len(options['days']) > 1:
            raise CommandError('More than one time period entered')

        days = options['days'][0]

        for i in range(1, days + 1):
            calc_statistics.delay(i)
            print("#### Task for calculating %s days before report created" % i)
