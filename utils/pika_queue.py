import logging

from json import dumps
from queue import Queue, Empty
from pika import BlockingConnection, URLParameters
from pika.exceptions import ConnectionClosed, ChannelClosed


class ConnectionQueue(Queue):
    def __init__(self, queue_name, params, maxsize=0, logger=None):
        super(ConnectionQueue, self).__init__(maxsize)
        if isinstance(params, URLParameters):
            self.params = params
        else:
            self.params = URLParameters(params)

        if logger is None:
            self.logger = logging.getLogger(__name__)
        elif isinstance(logger, str):
            self.logger = logging.getLogger(logger)
        else:
            self.logger = logger

        self.queue_name = queue_name

    def get(self, block=False, timeout=None):
        try:
            connection = super(ConnectionQueue, self).get(block, timeout)
            if connection.is_closed or connection.is_closing:
                self.logger.error("connection closed retrying again")
                return self.get(block, timeout)
            channel =  connection.channel()
            return channel
        except Empty:
            self.logger.info("block time out happened, create new connection")
            channel =  BlockingConnection(self.params).channel()
            return channel
        except ConnectionClosed:
            return self.get(block, timeout)

    def publish(self, msg):
        try:
            channel = self.get()
            channel.queue_declare(queue=self.queue_name, durable=True)
            channel.basic_publish(exchange='', routing_key=self.queue_name, body=dumps(msg))
            channel.close()
            self.put(channel.connection)
        except ChannelClosed:
            self.logger.info("channel already closed")
            self.put(channel.connection)
        except Exception as exc:
            self.logger.error('could not publish message %s, error: %s' % (msg, str(exc)))
