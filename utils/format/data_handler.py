from collections import OrderedDict
from datetime import datetime
from bson import ObjectId
from django.conf import settings


def bson2dict(data):
    if data:
        for k, v in data.items():
            if isinstance(v, ObjectId):
                data[k] = str(v)
        return data
    return {}


def clear_list(obj_list):
    # Quick heavy way
    return [bson2dict(obj) for obj in obj_list]

    # Soft slow way
    # for obj in obj_list:
    #     yield bson2dict(obj)


def clean_time(time):
    if isinstance(time, datetime):
        return time.isoformat()
    return time


def filter_by_service(collections, services):
    # collections = extract_service_names(collections)
    return filter(lambda col: col.split('_')[0] in services, collections)


def is_constant(data):
    if '_device_uuid' not in data.keys():
        return data['_device_uuid'] is None
    return False


def paginate_list(request, obj_list, obj_count):
    if 'p' in request.GET:
        page = int(request.GET.get('p'))
        if not page > 0:
            page = 1
        if obj_count > settings.PAGE_SIZE:
            result = obj_list[
                     settings.PAGE_SIZE * (page - 1):settings.PAGE_SIZE * page]
        else:
            result = obj_list[:settings.PAGE_SIZE]
    else:
        result = obj_list[:settings.PAGE_SIZE]
        page = 1

    return OrderedDict([
        ('total_objects', obj_count),
        ('total_pages', (obj_count // settings.PAGE_SIZE) + 1),
        ('current', page),
        ('results', clear_list(result))
    ])
