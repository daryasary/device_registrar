import requests

from uuid import uuid4
from time import sleep

from django.urls import reverse
from django.conf import settings

from services.models import Service


def fill_rabbitmq(count):
    body = {
        'client_os': 'android v4',
        'client_model': 'samsung galaxy note 4',
        'device_uuid': str(uuid4()),
        'event_data': [{'key': 'value'}]
    }

    headers = {
        'Authorization': 'JWT %s' % Service.objects.get(id=1).get_jwt_token(),
    }
    url = settings.BASE_URL + reverse('device-event-logger')
    for i in range(count):
        response = requests.post(url=url, json=body, headers=headers)
        print(response)
        if response.status_code == 429:
            sleep(61)
            i -= 1
