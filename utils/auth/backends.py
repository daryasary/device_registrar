from django.utils.six import text_type
from rest_framework import HTTP_HEADER_ENCODING, exceptions
from rest_framework.authentication import BaseAuthentication
from django.utils.translation import ugettext_lazy as _

from devices.models import Device


def get_device_registrar_header(request):
    device = request.META.get('HTTP_DEVICE_UUID', b'')
    if isinstance(device, text_type):
        device = device.encode(HTTP_HEADER_ENCODING)
    return device


class DeviceRegistrarAuthentication(BaseAuthentication):
    keyword = 'DRG'
    model = None

    def authenticate(self, request):
        auth = get_device_registrar_header(request).split()

        if not auth or auth[0].lower() != self.keyword.lower().encode():
            return None

        if len(auth) == 1:
            msg = _('Invalid token header. No credentials provided.')
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = _(
                'Invalid token header. Token string '
                'should not contain spaces.'
            )
            raise exceptions.AuthenticationFailed(msg)

        try:
            token = auth[1].decode()
        except UnicodeError:
            msg = _(
                'Invalid token header. Token string should '
                'not contain invalid characters.'
            )
            raise exceptions.AuthenticationFailed(msg)

        return self.authenticate_credentials(token)

    def authenticate_credentials(self, duid):
        from django.contrib.auth.models import AnonymousUser
        try:
            token = Device.objects.get(generated_uuid=duid)
        except Device.DoesNotExist:
            user = AnonymousUser()
            token = None
        else:
            user = AnonymousUser()
        user.device = token
        return user, token


authenticate_device = DeviceRegistrarAuthentication().authenticate
