from django.db import models
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _


User = get_user_model()


class ObjectsPermission(models.Model):
    READ = 1
    WRITE = 5
    DELETE = 10
    PERMISSION_CHOICES = (
        (READ, "Read"),
        (WRITE, "Write"),
        (DELETE, "Delete")
    )
    created_time = models.DateTimeField(_("Created time"), auto_now_add=True)
    updated_time = models.DateTimeField(_("Modified time"), auto_now=True)
    user = models.ForeignKey(User, verbose_name=_("User"), related_name="permissions")
    object_id = models.CharField(max_length=64, verbose_name=_("Object id"))
    permission = models.IntegerField(choices=PERMISSION_CHOICES, default=READ)
    valid = models.BooleanField(default=True, verbose_name=_('Valid'))
    expire_time = models.DateTimeField(_("Expiration date"), blank=True, null=True)

    def __str__(self):
        return '{} > {} > {}'.format(self.user, self.object_id, self.permission)