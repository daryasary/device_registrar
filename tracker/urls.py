from django.conf.urls import url
from tracker.views import adjust_url

urlpatterns = [
    url(r'(?P<url_code>\w+)?$', adjust_url, name='adjust-url')
]

