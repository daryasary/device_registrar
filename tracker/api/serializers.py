from rest_framework import serializers
from tracker.models import AdjustURL


class AdjustUrlSerializer(serializers.ModelSerializer):
    related_url = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = AdjustURL
        fields = "__all__"

    def get_related_url(self,instance):
            return instance.related_url

