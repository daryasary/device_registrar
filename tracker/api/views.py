from rest_framework import mixins
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from tracker.api.serializers import AdjustUrlSerializer
from tracker.models import AdjustURL


class UrlsViewSet(mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  mixins.CreateModelMixin,
                  viewsets.GenericViewSet):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return AdjustURL.objects.filter(service__user=self.request.user, **{
            "service_id": self.request.query_params.get("service")} if (
                    "service" in self.request.query_params and self.request.query_params.get("service")) else {})

    serializer_class = AdjustUrlSerializer
