from rest_framework import routers

from tracker.api.views import UrlsViewSet

router = routers.SimpleRouter()
router.register(r'related_urls', UrlsViewSet, "related_urls")

urlpatterns = router.urls
