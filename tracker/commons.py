from django.core.cache import cache
from .models import AdjustURL


def get_from_cache(url_id):
    """
    check url id in cache server if exists return it if not retrieve it from db and set a key with tracker prefix
     on cache server
    :param url_id: id of url
    :return: related url to id
    """
    related_url = cache.get("tracker_%s" % url_id, None)
    if related_url is None:
        try:
            url_obj = AdjustURL.objects.get(id=url_id)
            related_url = url_obj.destination_url
            assert related_url
        except Exception as e:
            return None
        cache.set("tracker_%s" % url_id, related_url)

    return related_url


def get_client_ip(request):
    try:
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        return ip
    except Exception as e:
        return None
