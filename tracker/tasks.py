from celery import shared_task
from .models import AdjustEvent


@shared_task
def save_adjust_event(event_type, data, device_uuid, adjust_url, created_time,local_ip,public_ip):
    AdjustEvent.objects.create(event_type=event_type, event_data=data, device_uuid=device_uuid,
                               adjust_url_id=adjust_url, created_time=created_time,local_ip=local_ip,public_ip=public_ip)
