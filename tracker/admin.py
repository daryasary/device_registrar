from django.contrib import admin
from django.utils.html import format_html_join

from .models import AdjustURL, AdjustEvent


@admin.register(AdjustURL)
class AdjustURLAdmin(admin.ModelAdmin):
    list_display = ('title', 'destination_url', 'service', 'show_url_code')

    def show_url_code(self, instance):
        return instance.url_code(instance.pk)
    show_url_code.short_description = 'URL_CODE'


@admin.register(AdjustEvent)
class AdjustEventAdmin(admin.ModelAdmin):
    list_display = ('id', 'event_type', 'event_data', 'get_adjust_url', 'created_time')
    list_filter = ('created_time',)
    # readonly_fields = ('related_url')

    def get_adjust_url(self, obj):
        return obj.adjust_url.destination_url if obj.adjust_url else None
    get_adjust_url.short_description = 'DESTINATION URL'
    get_adjust_url.admin_order_field = 'adjust_url'

