import socket
import struct
from django.core.cache import cache

from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils.translation import ugettext_lazy as _
from utils.shortner import UrlEncoder
from services.models import Service


class AdjustURL(models.Model):
    created_time = models.DateTimeField(_("Created time"), auto_now_add=True)
    updated_time = models.DateTimeField(_("Modified time"), auto_now=True)
    title = models.CharField(_("title"), max_length=150, default='')
    destination_url = models.URLField(_("Destination url"), max_length=200)
    service = models.ForeignKey(Service, related_name="adjust_url")

    class Meta:
        db_table = 'tracker_urls'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._b_destination_url = self.destination_url

    @staticmethod
    def url_code(record_id):
        return UrlEncoder().encode_id(record_id)

    @property
    def related_url(self):
        return "%s/links/%s" % (settings.BASE_URL, self.url_code(self.id))

    def save(self, **kwargs):
        super(AdjustURL, self).save(**kwargs)
        cache.set("tracker_%s" % self.id, self.destination_url)

    def delete(self, **kwargs):
        super(AdjustURL, self).delete(**kwargs)
        cache.delete("tracker_%s" % self.id)


class AdjustEvent(models.Model):
    CLICK = 1
    INSTALL = 2
    TYPE_CHOICES = (
        (CLICK, _('click')),
        (INSTALL, _('install')),
    )
    created_time = models.DateTimeField(_("Created time"), auto_now_add=True, db_index=True)
    device_uuid = models.UUIDField(db_index=True, null=True, blank=True)
    event_type = models.PositiveSmallIntegerField(_('Event type'), choices=TYPE_CHOICES)
    adjust_url = models.ForeignKey(AdjustURL, related_name="adjust_event", null=True, blank=True)
    local_ip = models.BigIntegerField(_("local ip"), db_index=True, null=True, blank=True)
    public_ip = models.BigIntegerField(_("public ip"), db_index=True, null=True, blank=True)
    event_data = JSONField(_("Event data"))

    class Meta:
        db_table = 'tracker_events'

    @staticmethod
    def ip_to_int(addr):
        return struct.unpack("!I", socket.inet_aton(addr))[0]

    @staticmethod
    def int_to_ip(addr):
        return socket.inet_ntoa(struct.pack("!I", addr))
