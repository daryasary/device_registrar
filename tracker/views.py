import json
from datetime import datetime

from django.http import HttpResponse, HttpResponseNotFound
from django.template import loader
from rest_framework.decorators import api_view

from tracker.commons import get_from_cache, get_client_ip
from utils.shortner import UrlEncoder
from .models import AdjustEvent
from .tasks import save_adjust_event

url_encoder = UrlEncoder()


@api_view(['POST', 'GET'])
def adjust_url(request, url_code):
    """
    this a main function for adjust tracker system API
    this view support post and get, they will call one after another . scenario is client open a web page(GET) and
    web page run a js script client side and immediately call server(POST) to register data that grabbed by js script.

    two different type of call we considered:

        1- click type : call by link url and user click in common browsers
            - this type always has url_code because its origin is complete link
            - this type include no device_uuid because only installed apps have registered uuid

        2- install type: call by app(sdk) to notify a device install
            - this type include no url_code
            - this type always include device_uuid
            - this type always include package_name
            - this type can has google_source: google source is a direct way to link a click event to install event


    :param request: user request object
    :param url_code: an url encoded word that is converted id of related url.
    :return:
    """
    url_id = url_encoder.decode_id(url_code) if url_code else None
    related_url = get_from_cache(url_id) if url_id else None

    if request.data:

        event_type = AdjustEvent.CLICK if url_id else AdjustEvent.INSTALL

        if not url_id and "google_source" in request.data:
            url_id = url_encoder.decode_id(request.data["google_source"])
            related_url = get_from_cache(url_id) if url_id else None

        public_ip = AdjustEvent.ip_to_int(get_client_ip(request))
        save_adjust_event.delay(event_type=event_type,
                                device_uuid=request.data.pop("device_uuid") if 'device_uuid' in request.data else None,
                                adjust_url=url_id if related_url else None,
                                created_time=datetime.now(),
                                public_ip=public_ip,
                                local_ip=AdjustEvent.ip_to_int(
                                    request.data.pop("local_IP")) if 'local_IP' in request.data else None,
                                data=json.dumps(request.data))

        return HttpResponse()

    if not related_url and url_code:
        return HttpResponseNotFound()

    template = loader.get_template('tracker/redirect_js.html')
    return HttpResponse(template.render(
        {"destination_url": related_url,
         "device_uuid": request.query_params.get("device_uuid", None),
         "package_name": request.query_params.get("package_name", None),
         "google_source": request.query_params.get("google_source", None)}, request))
