import logging
import signal
import sys
from datetime import datetime, timedelta
from json import loads
from django.db import IntegrityError

import pika
from django.conf import settings
from django.core.management.base import BaseCommand

from devices.models import DeviceLog

logger = logging.getLogger('consumer')


class Command(BaseCommand):
    def __init__(self, **kwargs):
        super(Command, self).__init__(**kwargs)
        self.params = pika.URLParameters(settings.RBMQ_URL, )
        self.connection = pika.BlockingConnection(self.params)
        self.last_tag = 0
        self.device_log_list = list()
        self.last_time = datetime.now()
        signal.signal(signal.SIGTERM, self.kill)

    def kill(self, signum, frame):
        logger.info('SIGTERM received')
        self.bulk_insert()
        self.close()
        sys.exit(0)

    def close(self):
        self.connection.close()

    def bulk_insert(self):
        if self.device_log_list:
            logger.info('bulk insert for %s item(s)' % len(self.device_log_list))
            try:
                DeviceLog.objects.bulk_create(self.device_log_list)
            except IntegrityError as e:
                """
                for obj in self.device_log_list:
                    try:
                        obj.save()
                    except IntegrityError:
                        logger.error('IntegrityError happened for object %s' % obj)
                        continue
                """
                logger.error('integrity error %s happened for list %s' % (str(e), self.device_log_list))
            except Exception as e:
                logger.error('exception %s happened for list %s' % (str(e), self.device_log_list))

        self.channel.basic_ack(delivery_tag=self.last_tag, multiple=True)

        self.last_tag = 0
        self.device_log_list = list()
        self.last_time = datetime.now()
        self.channel.close()

    def consumer(self):
        self.channel = self.connection.channel()
        for method_frame, properties, body in self.channel.consume(settings.RBMQ_ROUTE_KEY,
                                                                   inactivity_timeout=settings.MAX_TIME_INACTIVE):
            # Display the message parts and ack the message
            data = loads(body.decode())
            # logger.info('getting log from MQ, %s' % (data,))
            try:
                requested_time = datetime.strptime(data['requested_time'], "%Y-%m-%d %H:%M:%S")
            except Exception as e:
                requested_time = datetime.now()

            self.device_log_list.append(
                DeviceLog(
                    device_uuid=data['device_uuid'],
                    event_data=data['event_data'],
                    duration=datetime.now() - requested_time,
                    service_id=int(data['service_id']),
                    event_uuid=data['event_uuid']
                )
            )
            self.last_tag = method_frame.delivery_tag
            if self.last_tag >= settings.MAX_LOG_COUNT or \
                            (datetime.now() - self.last_time).total_seconds() >= settings.MAX_TIME_WAIT:
                logger.info('limit reached, last_tag is %d, last time is %s' % (self.last_tag, self.last_time))
                break

        self.bulk_insert()

    def handle(self, *args, **options):
        while True:
            try:
                self.consumer()
            except KeyboardInterrupt:
                logger.info('keyboard interrupt received')
                self.bulk_insert()
                break
            except TypeError:
                self.bulk_insert()
        self.close()
