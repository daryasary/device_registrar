from time import sleep
from uuid import uuid4

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.cache import cache
from django.shortcuts import resolve_url
from django.test import TestCase

from rest_framework.test import APIClient
from devices.management.commands.consumer import Command
from devices.models import Device, DeviceOS, DeviceModel, DeviceLog
from services.models import Service

User = get_user_model()
settings.configure()

class RegisterTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create(
            username='test_user', email='test@test.com',password='test123'
        )
        cls.service = Service.objects.create(package_name='test package', package_version=1, user=cls.user)
        cls.body = {
            'client_os': 'android v4',
            'client_model': 'samsung galaxy note 4',
            'device_uid': 'f07a13984f6d116a',
            'extra_data': None
        }

    def test_registration(self):
        cache.clear()
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT %s' % self.service.get_jwt_token())

        response = client.post(resolve_url('device-register'), self.body, format='json')
        self.assertEqual(201, response.status_code, 'request was not successful')
        try:
            device = Device.objects.get(device_uid='f07a13984f6d116a')
        except Device.DoesNotExist:
            self.assert_(msg='device does not created successfully')
        self.assertEqual(device.device_uid, response.data.get('device_uid'), msg='device uids dosent match')
        cache.clear()


class ThrottleTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create(
            username='test_user', email='test@test.com',password='test123'
        )
        cls.service = Service.objects.create(package_name='test package', package_version=1, user=cls.user)

    def test_register(self):
        body = {
            'client_os': 'android v4',
            'client_model': 'samsung galaxy note 4',
            'device_uid': 'f07a13984f6d116a',
            'extra_data': None
        }
        cache.clear()
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT %s' % self.service.get_jwt_token())
        for i in range(3):
            response = client.post(resolve_url('device-register'), body, format='json')
            self.assertEqual(201, response.status_code, 'request was not successful for %dnth request' % i)

        response = client.post(resolve_url('device-register'), body, format='json')
        self.assertEqual(429, response.status_code, 'request was not blocked')

        sleep(60)

        response = client.post(resolve_url('device-register'), body, format='json')
        self.assertEqual(201, response.status_code, 'request still blocked')
        cache.clear()

    def test_logger(self):
        body = {
            'client_os': 'android v4',
            'client_model': 'samsung galaxy note 4',
            'device_uuid': uuid4(),
            'event_data': [{'key': 'value'}]
        }
        cache.clear()
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT %s' % self.service.get_jwt_token())

        for i in range(50):
            response = client.post(resolve_url('device-event-logger'), body, format='json')
            self.assertEqual(201, response.status_code, 'request was not successful for %dnth request' % i)
        response = client.post(resolve_url('device-event-logger'), body, format='json')
        self.assertEqual(429, response.status_code, 'request was not blocked')


class CacheTest(TestCase):
    def test_device_model_create(self):
        cache.clear()
        device_model = DeviceModel.objects.create(name='samsung galaxy note 8')
        model_cache = cache.get(settings.CACHE_KEY_MODEL, dict())
        self.assertTrue(
            device_model.name in model_cache,
            msg='model created but not inserted in cache, cache content: %s' % model_cache
        )
        cache.clear()

    def test_device_os_create(self):
        cache.clear()
        device_os = DeviceOS.objects.create(name='android v6')
        os_cache = cache.get(settings.CACHE_KEY_OS, dict())
        self.assertTrue(
            device_os.name in os_cache,
            msg='model created but not inserted in cache, cache content: %s' % os_cache
        )
        cache.clear()

    def test_os_remove(self):
        cache.clear()
        device_os = DeviceOS.objects.create(name='android v6')
        device_os.delete()
        os_cache = cache.get(settings.CACHE_KEY_OS, dict())
        self.assertTrue(
            device_os.name not in os_cache,
            msg='model deleted but not removed from cache, cache content: %s' % os_cache
        )
        cache.clear()

    def test_model_remove(self):
        cache.clear()
        device_model = DeviceModel.objects.create(name='samsung galaxy note 8')
        device_model.delete()
        model_cache = cache.get(settings.CACHE_KEY_MODEL, dict())
        self.assertTrue(
            device_model.name not in model_cache,
            msg='model deleted but not removed from cache, cache content: %s' % model_cache
        )
        cache.clear()

    def test_os_change(self):
        cache.clear()
        device_os = DeviceOS.objects.create(name='android v6')
        device_os.name = 'android v5'
        device_os.save()
        os_cache = cache.get(settings.CACHE_KEY_OS, dict())
        self.assertTrue(
            device_os.name in os_cache,
            msg='os changed but not changed in cache, cache content: %s' % os_cache
        )
        cache.clear()

    def test_model_change(self):
        cache.clear()
        device_model = DeviceModel.objects.create(name='samsung galaxy note 8')
        device_model.name = 'htc one x'
        device_model.save()
        model_cache = cache.get(settings.CACHE_KEY_MODEL, dict())
        self.assertTrue(
            device_model.name in model_cache,
            msg='model changed but not changed in cache, cache content: %s' % model_cache
        )
        cache.clear()


class CustomJWTTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create(
            username='test_user', email='test@test.com', password='test123'
        )
        cls.service = Service.objects.create(package_name='test package', package_version=1, user=cls.user)

    def test_unvalid_token(self):
        body = {
            'client_os': 'android v4',
            'client_model': 'samsung galaxy note 4',
            'device_uuid': uuid4(),
            'event_data': [{'key': 'value'}]
        }
        self.service.delete()
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='JWT %s' % self.service.get_jwt_token())
        response = client.post(resolve_url('device-event-logger'), body, format='json')
        self.assertEqual(401, response.status_code, 'request was not blocked')


class ConsumerTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create(
            username='test_user', email='test@test.com', password='test123'
        )
        cls.service = Service.objects.create(package_name='test package', package_version=1, user=cls.user)

    def test_consumer(self):
        body = {
            'client_os': 'android v4',
            'client_model': 'samsung galaxy note 4',
            'device_uuid': '37373426-32d4-42e4-8aa4-6ed47d355def',
            'event_data': [{'count': 0,'uuid':'8f6e7b6c-00ee-11e8-ba89-0ed5f89f718b'},{'count': 0,'uuid':'8f6e7eb4-00ee-11e8-ba89-0ed5f89f718b'},{'count': 0,'uuid':'8f6e806c-00ee-11e8-ba89-0ed5f89f718b'},{'count': 0,'uuid':'78ce1594-ff15-4385-aeca-6ae9b7fc81a3'}]
        }

        for i in range(settings.MAX_LOG_COUNT):
            client = APIClient()
            client.credentials(HTTP_AUTHORIZATION='JWT %s' % self.service.get_jwt_token())
            response = client.post(resolve_url('device-event-logger'), body, format='json')
            if response.status_code == 429:
                i -= 1
                sleep(61)
                continue
            body['event_data'][0]['count'] += 1
            self.assertEqual(201, response.status_code, 'request was not successful')
        Command().consumer()
        result = DeviceLog.objects.all().order_by('id')
        print(result)
        count_checker = 0
        for i in range(settings.MAX_LOG_COUNT):
            self.assertEqual(body.get('device_uuid'), str(result[i].device_uuid), msg='object was not inserted')
            self.assertEqual(result[i].event_data.get('count'), count_checker)
            count_checker += 1
            print('checked')
