import logging

from django.conf import settings
from django.core.cache import cache
from django.db.models.signals import post_save, post_delete, post_migrate
from django.dispatch import receiver

from .models import DeviceModel, DeviceOS
from .views import cache_os_init, cache_model_init

logger = logging.getLogger('register_info')


@receiver(post_save, sender=DeviceOS)
def device_os_post_save(sender, instance, created, **kwargs):
    # Do not read cache if name field has not changed
    if not created:
        if instance._b_name == instance.name:
            return
    devices_os = cache_os_init()
    if not created:
        devices_os.pop(instance.name, None)
    devices_os[instance.name] = instance.id
    cache.set(settings.CACHE_KEY_OS, devices_os)


@receiver(post_save, sender=DeviceModel)
def device_model_post_save(sender, instance, created, **kwargs):
    # Do not read cache if name field has not changed
    if not created:
        if instance._b_name == instance.name:
            return
    device_model = cache_model_init()
    if not created:
        device_model.pop(instance._b_name, None)
    device_model[instance.name] = instance.id
    cache.set(settings.CACHE_KEY_MODEL, device_model)


@receiver(post_delete, sender=DeviceOS)
def device_os_post_delete(sender, instance, **kwargs):
    devices_os = cache_os_init()
    devices_os.pop(instance.name, None)
    cache.set(settings.CACHE_KEY_OS, devices_os)


@receiver(post_delete, sender=DeviceModel)
def device_model_post_delete(sender, instance, **kwargs):
    devices_model = cache_model_init()
    devices_model.pop(instance.name, None)
    cache.set(settings.CACHE_KEY_MODEL, devices_model)


@receiver(post_migrate)
def clear_cache(**kwargs):
    cache.delete(settings.CACHE_KEY_OS)
    cache.delete(settings.CACHE_KEY_MODEL)
