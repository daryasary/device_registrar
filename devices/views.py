import logging

from django.conf import settings
from django.core.cache import cache

from devices.models import DeviceModel, DeviceOS

reg_logger = logging.getLogger('register')


def cache_os_init():
    devices_os = cache.get(settings.CACHE_KEY_OS, dict())
    if not devices_os:
        for dos in DeviceOS.objects.all().values():
            devices_os[dos['name']] = dos['id']
        reg_logger.info('initialization os cache with content %s' % devices_os)
        cache.set(settings.CACHE_KEY_OS, devices_os)
    return devices_os


def cache_model_init():
    devices_model = cache.get(settings.CACHE_KEY_MODEL, dict())
    if not devices_model:
        for d_model in DeviceModel.objects.all().values():
            devices_model[d_model['name']] = d_model['id']
        reg_logger.info('initialization model cache with content %s' % devices_model)
        cache.set(settings.CACHE_KEY_MODEL, devices_model)
    return devices_model
