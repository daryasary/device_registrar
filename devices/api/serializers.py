import logging

from django.utils.text import ugettext_lazy as _
from rest_framework import serializers

from devices.models import Device, DeviceModel, DeviceOS, DeviceMembership, DeviceLog
from devices.views import cache_model_init, cache_os_init


reg_logger = logging.getLogger('register')


class UpdateNotifyTokenSerializer(serializers.ModelSerializer):
    onesignal_token = serializers.CharField(max_length=40, required=False)
    fcm_token = serializers.CharField(max_length=40, required=False)

    class Meta:
        model = DeviceMembership
        fields = ('onesignal_token', 'fcm_token')

    def validate(self, data):
        if not (data.get('onesignal_token') or data.get('onesignal_token')):
            serializers.ValidationError(_('Nothing to update'))
        return data

    def update(self, instance, validated_data):
        instance.onesignal_token = validated_data.get('onesignal_token', instance.onesignal_token)
        instance.fcm_token = validated_data.get('fcm_token', instance.fcm_token)
        instance.save(update_fields=['onesignal_token', 'fcm_token'])
        return instance


class RegisterSerializer(serializers.ModelSerializer):
    client_os = serializers.CharField(max_length=10, write_only=True, required=True, allow_blank=False)
    client_model = serializers.CharField(max_length=64, write_only=True, required=True, allow_blank=False)
    extra_data = serializers.JSONField(required=True)
    onesignal_token = serializers.CharField(max_length=40, write_only=True, required=False, allow_blank=True)
    fcm_token = serializers.CharField(max_length=40, write_only=True, required=False, allow_blank=True)

    class Meta:
        model = Device
        fields = (
            'generated_uuid', 'device_uid', 'client_os', 'client_model', 'onesignal_token', 'fcm_token', 'extra_data'
        )
        read_only_fields = ('generated_uuid',)

    def validate_client_os(self, value):
        devices_os = cache_os_init()
        device_os_id = devices_os.get(value)
        if device_os_id is None:
            try:
                device_os_id = DeviceOS.objects.create(name=value).id
            except Exception as exc:
                reg_logger.error('create os model name: %s, Exception %s' % (value, str(exc)))
                raise serializers.ValidationError(_('could not accept os with name %s') % value)
        # reg_logger.info('os cache updated with content %s ' % devices_os)
        return device_os_id

    def validate_client_model(self, value):
        devices_model = cache_model_init()
        devices_model_id = devices_model.get(value)
        if devices_model_id is None:
            try:
                devices_model_id = DeviceModel.objects.create(name=value).id
            except Exception as exc:
                reg_logger.error('create device model with name: %s, Exception %s' % (value, str(exc)))
                raise serializers.ValidationError(_('could not accept device model with name %s') % value)
        # reg_logger.info('model cache updated with content %s ' % devices_model)
        return devices_model_id

    def create(self, validated_data):
        try:
            service_id = self.context['request'].auth.get('service_id', 0)
            device, device_created = Device.objects.get_or_create(
                device_uid=validated_data['device_uid'],
                device_os_id=validated_data['client_os'],
                device_model_id=validated_data['client_model'],
            )

            if device_created or \
                    not DeviceMembership.objects.filter(device=device, service_id=service_id).exists():
                DeviceMembership.objects.create(
                    service_id=service_id,
                    device=device,
                    onesignal_token=validated_data.get('onesignal_token', ''),
                    fcm_token=validated_data.get('fcm_token', ''),
                    extra_data=validated_data['extra_data'],
                )

            device.extra_data = validated_data['extra_data']

            reg_logger.debug('service: %s, data received: %s' % (service_id, validated_data))
        except Exception as e:
            reg_logger.error('registration request_data: %s, JWT: %s, Exception: %s' % (
                self.context['request'].data, self.context['request'].auth, str(e)
            ))
            raise serializers.ValidationError(_('could not register client device'))

        return device


class LogSerializer(serializers.Serializer):
    device_uuid = serializers.UUIDField(required=True)
    event_data = serializers.ListField(child=serializers.JSONField(), write_only=True)

    def update(self, instance, validated_data):
        return

    def create(self, validated_data):
        return
