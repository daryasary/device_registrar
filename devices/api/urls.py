from django.conf.urls import url

from .views import DeviceRegister, EventLogger,UpdateDeviceMembership


urlpatterns = [
    url(r'^register/$', DeviceRegister.as_view(), name='device-register'),
    url(r'^update/(?P<device_uuid>[\w-]+)/$', UpdateDeviceMembership.as_view(), name='device-update-membership'),
    url(r'^logger/$', EventLogger.as_view(), name='device-event-logger'),
]
