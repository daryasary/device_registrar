import logging

from datetime import datetime

from django.conf import settings
from django.http import Http404

from rest_framework.generics import CreateAPIView, UpdateAPIView
from rest_framework.permissions import IsAuthenticated

from .authentications import JSONWebTokenAuthentication
from .serializers import RegisterSerializer, LogSerializer, UpdateNotifyTokenSerializer
from .throttles import LoggerThrottle, RegisterThrottle
from ..models import Device, DeviceMembership
from utils.pika_queue import ConnectionQueue

event_logger = logging.getLogger('rbmq')
connections = ConnectionQueue(settings.RBMQ_ROUTE_KEY, settings.RBMQ_URL, logger=event_logger)


class UpdateDeviceMembership(UpdateAPIView):
    """
    Update notify_token for given device_uuid in Membership table
    """
    serializer_class = UpdateNotifyTokenSerializer
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    throttle_scope = 'register'
    lookup_field = 'device'
    lookup_url_kwarg = 'device_uuid'

    def get_queryset(self):
        return DeviceMembership.objects.filter(service_id=self.request.auth.get('service_id'))

    def get_object(self):
        queryset = self.get_queryset()
        filter_kwargs = {self.lookup_field: self.kwargs[self.lookup_url_kwarg]}
        try:
            return queryset.get(**filter_kwargs)
        except DeviceMembership.DoesNotExist:
            raise Http404


class DeviceRegister(CreateAPIView):
    """
    Register a device & retrieve a UUID for it
    - extra_data can contain extra information of the device in the form of JSON
    """
    serializer_class = RegisterSerializer
    queryset = Device.objects.all()
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    throttle_classes = (RegisterThrottle,)
    throttle_scope = 'register'


class EventLogger(CreateAPIView):
    """
    Set Log for a device with Provided UUID
    - event_data field is a list of JSON of your taste (JSON List Field)
    - This API is for single event log object
    """
    serializer_class = LogSerializer
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    throttle_classes = (LoggerThrottle,)
    throttle_scope = 'logger'

    def perform_create(self, serializer):
        for item in serializer.validated_data['event_data']:
            if not item:
                event_logger.warning('got empty json for device %s in service %s' % (
                    str(serializer.validated_data['device_uuid']), self.request.auth.get('service_id')
                ))
                continue
            msg = {
                'device_uuid': str(serializer.validated_data['device_uuid']),
                'event_data': item,
                'service_id': self.request.auth.get('service_id', 0),
                'event_uuid': item['uuid']
            }
            msg['requested_time'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

            try:
                connections.publish(msg)
            except Exception as exc:
                event_logger.error('could not publish message %s, error: %s' % (msg, str(exc)))
