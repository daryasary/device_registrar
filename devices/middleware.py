from django.conf import settings
from django.utils.deprecation import MiddlewareMixin
from django.utils.functional import SimpleLazyObject

from utils.auth.backends import authenticate_device


def get_device(request):
    if not hasattr(request, '_cached_device'):
        request._cached_device = authenticate_device(request)
    return request._cached_device

    
class DeviceRegistrarMiddleware(MiddlewareMixin):
    def process_request(self, request):
        assert hasattr(request, 'session'), (
            "Device registration middleware requires session middleware "
            "to be installed. Edit your MIDDLEWARE%s setting to insert "
            "'django.contrib.sessions.middleware.SessionMiddleware' before "
            "'django.contrib.auth.middleware.DeviceRegistrarMiddleware'."
        ) % ("_CLASSES" if settings.MIDDLEWARE is None else "")
        request.device = SimpleLazyObject(lambda: get_device(request))
