import uuid
# from datetime import datetime

from django.contrib.postgres import fields
from django.db import models
from django.utils.translation import ugettext_lazy as _


class DeviceOS(models.Model):
    created_time = models.DateTimeField(_('created time'), auto_now_add=True)
    name = models.CharField(max_length=10, unique=True)

    class Meta:
        db_table = 'devices_os'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._b_name = self.name

    def __str__(self):
        return self.name


class DeviceModel(models.Model):
    created_time = models.DateTimeField(_('created time'), auto_now_add=True)
    name = models.CharField(max_length=64, unique=True)

    class Meta:
        db_table = 'devices_models'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._b_name = self.name

    def __str__(self):
        return self.name


class Device(models.Model):
    generated_uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_time = models.DateTimeField(auto_now_add=True)
    device_uid = models.CharField(max_length=40, db_index=True)
    device_os = models.ForeignKey(DeviceOS)
    device_model = models.ForeignKey(DeviceModel)
    service = models.ManyToManyField('services.Service', through='DeviceMembership')

    class Meta:
        db_table = 'devices'
        index_together = ('device_uid', 'device_os', 'device_model')

    def __str__(self):
        return str(self.generated_uuid)


class DeviceMembership(models.Model):
    created_time = models.DateTimeField(_('created time'), auto_now_add=True)
    device = models.ForeignKey(Device)
    service = models.ForeignKey('services.Service')
    onesignal_token = models.CharField(_('onesignal token'), max_length=40, blank=True)
    fcm_token = models.CharField(_('FCM token'), max_length=40, blank=True)
    extra_data = fields.JSONField(blank=True, null=True)

    class Meta:
        db_table = 'devices_membership'
        unique_together = ('device', 'service')

    def __str__(self):
        return '%s [%s]' % (self.device_id, self.service_id)


class DeviceLog(models.Model):
    created_time = models.DateTimeField(_('created time'), auto_now_add=True)
    device_uuid = models.UUIDField(db_index=True)
    event_uuid = models.UUIDField(unique=True, default=uuid.uuid4)
    duration = models.DurationField()
    event_data = fields.JSONField()
    service = models.ForeignKey('services.Service')

    class Meta:
        db_table = 'devices_logs'

    def __str__(self):
        return '%s, service: %s, %s' % (self.device_uuid, self.service_id, self.event_data)
