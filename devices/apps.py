from django.apps import AppConfig


class DeviceConfig(AppConfig):
    name = 'devices'

    def ready(self):
        import devices.signals