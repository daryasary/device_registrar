from django.contrib import admin

from .models import Device, DeviceLog, DeviceModel, DeviceOS


@admin.register(Device)
class DeviceAdmin(admin.ModelAdmin):
    list_display = ('generated_uuid', 'created_time', 'device_uid', 'device_os', 'device_model')
    list_filter = ('service', 'device_os')
    search_fields = ('generated_uuid', )


@admin.register(DeviceLog)
class DeviceLogAdmin(admin.ModelAdmin):
    list_display = ('created_time', 'device_uuid', 'duration', 'service')
    list_filter = ('service', )
    search_fields = ('device_uuid', )


@admin.register(DeviceOS)
class DeviceOSAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_time'
    search_fields = ('name', )


@admin.register(DeviceModel)
class DeviceModelAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_time'
    search_fields = ('name', )

